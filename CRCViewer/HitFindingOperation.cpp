#include "stdafx.h"
#include "HitFindingOperation.h"
#include <string.h>
#include "CTCParams.h"
#include "VersionNumber.h"
#include <math.h>

#define MAX_NUCLEUS_PIXELS	500
#define MIN_BLUE_CUTOFF		40
#define MIN_OVERLAPPED_PIXELS	5
#define MAX_POSITION_OFFSET	 5
#define MAXINT_12BIT 4096
#define MAXINT_14BIT 16384

CHitFindingOperation::CHitFindingOperation()
{
}

CHitFindingOperation::~CHitFindingOperation()
{
}

void CHitFindingOperation::FreeBlobList(vector<CBlobData *> *blobList)
{
	for (int i = 0; i < (int)blobList->size(); i++)
	{
		if ((*blobList)[i] != NULL)
			delete (*blobList)[i];
	}
	blobList->clear();
}

void CHitFindingOperation::FreeRGNList(vector<CRGNData *> *rgnList)
{
	for (int i = 0; i < (int)rgnList->size(); i++)
	{
		if ((*rgnList)[i] != NULL)
		{
			delete (*rgnList)[i];
			(*rgnList)[i] = NULL;
		}
	}
	(*rgnList).clear();
}

void CHitFindingOperation::ErosionOperation(unsigned char *inImage, unsigned char *outImage, int width, int height)
{
	memset(outImage, 0, sizeof(unsigned char) * width * height);

	for (int i = 1; i < (height - 1); i++)
	{
		for (int j = 1; j < (width - 1); j++)
		{
			bool keep = true;
			for (int k = -1; k < 2; k++)
			{
				for (int h = -1; h < 2; h++)
				{
					if (inImage[width * (i + k) + (j + h)] != (unsigned char)255)
					{
						keep = false;
						break;
					}
				}
				if (!keep)
					break;
			}
			if (keep)
				outImage[width * i + j] = (unsigned char)255;
		}
	}
}

void CHitFindingOperation::GetBlobBoundary(CBlobData *blob)
{
	unsigned char *temp1 = new unsigned char[blob->m_Width * blob->m_Height];
	unsigned char *temp2 = new unsigned char[blob->m_Width * blob->m_Height];
	memset(temp2, 0, sizeof(unsigned char) * blob->m_Width * blob->m_Height);
	for (int i = 0; i < (int)blob->m_Pixels->size(); i++)
	{
		int x = (int)((*blob->m_Pixels)[i] % blob->m_Width);
		int y = (int)((*blob->m_Pixels)[i] / blob->m_Width);
		temp2[blob->m_Width * y + x] = (unsigned char)255;
	}
	ErosionOperation(temp2, temp1, blob->m_Width, blob->m_Height);
	blob->m_Boundary->clear();
	for (int i = 0; i < blob->m_Height; i++)
	{
		for (int j = 0; j < blob->m_Width; j++)
		{
			if ((temp2[blob->m_Width * i + j] == (unsigned char)255) &&
				(temp1[blob->m_Width * i + j] == (unsigned char)0))
			{
				int pos = (unsigned int) (i * blob->m_Width + j);
				blob->m_Boundary->push_back(pos);
			}
		}
	}
	delete[] temp1;
	delete[] temp2;
}

void CHitFindingOperation::GetBlueBlobs(CRGNData *region, vector<POINT> *posList, bool scanMode)
{
	m_MSERDetector.GetBlobs(region->GetImage(BLUE_COLOR), region->GetBlobData(BLUE_COLOR));
	if (region->GetBlobData(BLUE_COLOR)->size() > 0)
	{
		int x0, y0;
		region->GetPosition(&x0, &y0);
		int blueThreshold = region->GetCPI(BLUE_COLOR) + MIN_BLUE_CUTOFF;
		for (int i = 0; i < (int)region->GetBlobData(BLUE_COLOR)->size(); i++)
		{
			CBlobData *blob = (*region->GetBlobData(BLUE_COLOR))[i];
			if ((blob->m_Pixels->size() < MAX_NUCLEUS_PIXELS) &&
				(blob->m_Threshold > blueThreshold))
			{
				POINT pt;
				pt.x = x0 + (blob->m_IntensityPos % blob->m_Width);
				pt.y = y0 + (blob->m_IntensityPos / blob->m_Width);
				posList->push_back(pt);
			}
		}
		if (scanMode)
			FreeBlobList(region->GetBlobData(BLUE_COLOR));
	}
}

int CHitFindingOperation::GetOverlapPixelCount(vector<int> *blob1, vector<int> *blob2)
{
	int count = 0;
	int index1 = 0;
	int index2 = 0;
	while ((index1 < (int)blob1->size()) && (index2 < (int)blob2->size()))
	{
		if ((*blob1)[index1] < (*blob2)[index2])
			index1++;
		else if ((*blob1)[index1] > (*blob2)[index2])
			index2++;
		else
		{
			count++;
			index1++;
			index2++;
		}
	}
	return count;
}

void CHitFindingOperation::MergeBlobs(CRGNData *hitData, PIXEL_COLOR_TYPE color, int blueBlobIndex, vector<int> *blueBlobIndexList)
{
	vector<int> connectedToBlue;
	CBlobData *blueBlob = (*hitData->GetBlobData(BLUE_COLOR))[blueBlobIndex];
	vector<int> compositeBlob;
	for (int i = 0; i < (int)blueBlob->m_Pixels->size(); i++)
		compositeBlob.push_back((*blueBlob->m_Pixels)[i]);
	int totalOverlaps = 0;
	bool connected = false;
	for (int i = 0; i < (int)hitData->GetBlobData(color)->size(); i++)
	{
		CBlobData *blob = (*hitData->GetBlobData(color))[i];
		int overlappedPixels = GetOverlapPixelCount(blueBlob->m_Pixels, blob->m_Pixels);
		if (overlappedPixels > MIN_OVERLAPPED_PIXELS)
		{
			if (!connected)
			{
				connected = true;
				blueBlobIndexList->push_back(blueBlobIndex);
			}
			totalOverlaps += overlappedPixels;
			GetCompositeBlob(&compositeBlob, blob->m_Pixels);
			connectedToBlue.push_back(i);
		}
	}

	if (connected)
	{
		bool addedNewBlob = true;
		while (addedNewBlob)
		{
			addedNewBlob = false;
			for (int i = 0; i < (int)hitData->GetBlobData(BLUE_COLOR)->size(); i++)
			{
				if (blobIncluded(blueBlobIndexList, i))
					continue;
				CBlobData *blob = (*hitData->GetBlobData(BLUE_COLOR))[i];
				int overlappedPixels = GetOverlapPixelCount(&compositeBlob, blob->m_Pixels);
				if (overlappedPixels > MIN_OVERLAPPED_PIXELS)
				{
					blueBlobIndexList->push_back(i);				
					totalOverlaps += overlappedPixels;
					GetCompositeBlob(&compositeBlob, blob->m_Pixels);
					addedNewBlob = true;
				}
			}
			if (addedNewBlob)
			{
				addedNewBlob = false;
				for (int i = 0; i < (int)hitData->GetBlobData(color)->size(); i++)
				{
					if (blobIncluded(&connectedToBlue, i))
						continue;
					CBlobData *blob = (*hitData->GetBlobData(color))[i];
					int overlappedPixels = GetOverlapPixelCount(&compositeBlob, blob->m_Pixels);
					if (overlappedPixels > MIN_OVERLAPPED_PIXELS)
					{
						connectedToBlue.push_back(i);
						totalOverlaps += overlappedPixels;
						GetCompositeBlob(&compositeBlob, blob->m_Pixels);
						addedNewBlob = true;
					}
				}
			}
		}
		CBlobData *blob = new CBlobData(SAMPLEFRAMEWIDTH, SAMPLEFRAMEHEIGHT);
		if (color == RED_COLOR)
		{
			hitData->m_RedBlueOverlaps = totalOverlaps;
			hitData->GetBlobData(RB_COLORS)->push_back(blob);
		}
		else
		{
			hitData->m_GreenBlueOverlaps = totalOverlaps;
			hitData->GetBlobData(GB_COLORS)->push_back(blob);
		}
		for (int i = 0; i < (int)compositeBlob.size(); i++)
			blob->m_Pixels->push_back(compositeBlob[i]);
		compositeBlob.clear();
		vector<CBlobData *> list;
		for (int i = 0; i < (int)hitData->GetBlobData(color)->size(); i++)
		{
			blob = (*hitData->GetBlobData(color))[i];
			(*hitData->GetBlobData(color))[i] = NULL;
			if (blobIncluded(&connectedToBlue, i))
				list.push_back(blob);
			else
				delete blob;
		}
		hitData->GetBlobData(color)->clear();
		for (int i = 0; i < (int)list.size(); i++)
		{
			hitData->GetBlobData(color)->push_back(list[i]);
			list[i] = NULL;
		}
		list.clear();
	}
}

void CHitFindingOperation::GetCompositeBlob(vector<int> *compositeBlob, vector<int> *blobPixels)
{
	vector<int> pixels;
	for (int i = 0; i < (int)compositeBlob->size(); i++)
		pixels.push_back((*compositeBlob)[i]);
	for (int i = 0; i < (int)blobPixels->size(); i++)
		pixels.push_back((*blobPixels)[i]);
	QuickSort(&pixels, 0, ((int)pixels.size() - 1));
	int index = 1;
	int lastPos = pixels[0];
	compositeBlob->clear();
	compositeBlob->push_back(lastPos);
	while (index < (int)pixels.size())
	{
		if (pixels[index] == lastPos)
			index++;
		else
		{
			lastPos = pixels[index];
			compositeBlob->push_back(lastPos);
			index++;
		}
	}
	pixels.clear();
}

bool CHitFindingOperation::blobIncluded(vector<int> *list, int blobIndex)
{
	bool ret = false;
	for (int i = 0; i < (int)list->size(); i++)
	{
		if (blobIndex == (*list)[i])
		{
			ret = true;
			break;
		}
	}

	return ret;
}

int CHitFindingOperation::GetBlueBlobIndex(CRGNData *hitData, POINT pos)
{
	int index = -1;
	if ((int)hitData->GetBlobData(BLUE_COLOR)->size() > 0)
	{
		int x0, y0;
		hitData->GetPosition(&x0, &y0);
		int x1 = (int)pos.x - x0;
		int y1 = (int)pos.y - y0;
		int difference = MAXINT;
		int minIndex = -1;
		for (int k = 0; k < (int)hitData->GetBlobData(BLUE_COLOR)->size(); k++)
		{
			CBlobData *blueBlob = (*hitData->GetBlobData(BLUE_COLOR))[k];
			int x2 = blueBlob->m_IntensityPos % blueBlob->m_Width;
			int y2 = blueBlob->m_IntensityPos / blueBlob->m_Width;
			if ((abs(x2 - x1) + abs(y2 - y1)) < difference)
			{
				difference = abs(x2 - x1) + abs(y2 - y1);
				minIndex = k;
			}
		}
		if (difference < MAX_POSITION_OFFSET)
			index = minIndex;
	}
	return index;
}

bool CHitFindingOperation::FindCompositeBlobs(CRGNData *hitData, POINT pos, vector<CBlobData *> *remainingBlobs)
{
	bool found = false;
	int blueBlobIndex = GetBlueBlobIndex(hitData, pos);
	if (blueBlobIndex > -1)
	{
		hitData->m_RedBlueOverlaps = 0;
		hitData->m_GreenBlueOverlaps = 0;
		vector<int> blueInRB;
		m_MSERDetector.GetBlobs(hitData->GetImage(RED_COLOR), hitData->GetBlobData(RED_COLOR));
		MergeBlobs(hitData, RED_COLOR, blueBlobIndex, &blueInRB);
		vector<int> blueInGB;
		m_MSERDetector.GetBlobs(hitData->GetImage(GREEN_COLOR), hitData->GetBlobData(GREEN_COLOR));
		MergeBlobs(hitData, GREEN_COLOR, blueBlobIndex, &blueInGB);
		if ((blueInRB.size() > 0) || (blueInGB.size() > 0))
			found = true;
		if (found)
		{
			vector<CBlobData *> list;
			for (int i = 0; i < (int)hitData->GetBlobData(BLUE_COLOR)->size(); i++)
			{
				CBlobData *blob = (*hitData->GetBlobData(BLUE_COLOR))[i];
				(*hitData->GetBlobData(BLUE_COLOR))[i] = NULL;
				if ((blobIncluded(&blueInRB, i)) || (blobIncluded(&blueInGB, i)))
					list.push_back(blob);
				else
				{
					if (remainingBlobs == NULL)
						delete blob;
					else
						remainingBlobs->push_back(blob);
				}
			}
			hitData->GetBlobData(BLUE_COLOR)->clear();
			for (int i = 0; i < (int)list.size(); i++)
			{
				hitData->GetBlobData(BLUE_COLOR)->push_back(list[i]);
				list[i] = NULL;
			}
			list.clear();
		}
	}
	return found;
}
template<typename T>
void CHitFindingOperation::QuickSort(vector<T> *list, int lo, int hi)
{
	if (lo < hi)
	{
		int position = Partition(list, lo, hi);
		QuickSort(list, lo, position - 1);
		QuickSort(list, position + 1, hi);
	}
}

int CHitFindingOperation::Partition(vector<int> *list, int lo, int hi)
{
	int position = ChoosePivot(list, lo, hi);
	int pos = (*list)[position];
	Swap(list, position, hi);
	int storeIndex = lo;
	for (int i = lo; i < hi; i++)
	{
		if ((*list)[i] < pos)
		{
			Swap(list, i, storeIndex);
			storeIndex++;
		}
	}
	Swap(list, storeIndex, hi);
	return storeIndex;
}

int CHitFindingOperation::ChoosePivot(vector<int> *list, int lo, int hi)
{
	int pivot = lo;
	int pos0 = (*list)[lo];
	int pos1 = (*list)[(lo + hi) / 2];
	int pos2 = (*list)[hi];
	if ((pos0 <= pos1) && (pos1 <= pos2))
		pivot = (lo + hi) / 2;
	else if ((pos0 <= pos2) && (pos2 <= pos1))
		pivot = hi;
	else
		pivot = lo;
	return pivot;
}

int CHitFindingOperation::Partition(vector<CRGNData *> *list, int lo, int hi)
{
	int position = ChoosePivot(list, lo, hi);
	double pos = (*list)[position]->m_Ratio1;
	Swap(list, position, hi);
	int storeIndex = lo;
	for (int i = lo; i < hi; i++)
	{
		if ((*list)[i]->m_Ratio1 < pos)
		{
			Swap(list, i, storeIndex);
			storeIndex++;
		}
	}
	Swap(list, storeIndex, hi);
	return storeIndex;
}

int CHitFindingOperation::ChoosePivot(vector<CRGNData *> *list, int lo, int hi)
{
	int pivot = lo;
	double pos0 = (*list)[lo]->m_Ratio1;
	double pos1 = (*list)[(lo + hi) / 2]->m_Ratio1;
	double pos2 = (*list)[hi]->m_Ratio1;
	if ((pos0 <= pos1) && (pos1 <= pos2))
		pivot = (lo + hi) / 2;
	else if ((pos0 <= pos2) && (pos2 <= pos1))
		pivot = hi;
	else
		pivot = lo;
	return pivot;
}

template<typename T>
void CHitFindingOperation::Swap(vector<T> *list, int index1, int index2)
{
	T ptr = (*list)[index1];
	(*list)[index1] = (*list)[index2];
	(*list)[index2] = ptr;
}

bool CHitFindingOperation::IsCircularBlob(CBlobData *blob)
{
	bool ret = false;
	
	int left = blob->m_Width;
	int right = 0;
	int top = blob->m_Height;
	int bottom = 0;
	for (int i = 0; i < (int)blob->m_Pixels->size(); i++)
	{
		int x = (*blob->m_Pixels)[i] % blob->m_Width;
		int y = (*blob->m_Pixels)[i] / blob->m_Width;
		if (x < left)
			left = x;
		if (x > right)
			right = x;
		if (y < top)
			top = y;
		if (y > bottom)
			bottom = y;
	}
	int width1 = (right - left + 1);
	int height1 = (bottom - top + 1);
	int longSide = width1;
	int shortSide = height1;
	if (height1 > longSide)
	{
		longSide = height1;
		shortSide = width1;
	}
	int aspectRatio = 100 * shortSide / longSide;
	int extent = 100 * (int)blob->m_Pixels->size() / longSide / shortSide;
	if ((aspectRatio > 35) && (extent > 50))
		ret = true;

	return ret;
}

int CHitFindingOperation::GetAverageIntensity(unsigned short *image, vector<int> *pixels)
{
	int intensity = 0;

	for (int i = 0; i < (int)pixels->size(); i++)
		intensity += image[(*pixels)[i]];

	if ((int)pixels->size() > 0)
		intensity /= (int)pixels->size();

	return intensity;
}

void CHitFindingOperation::CalculateBoundary(CRGNData *hitData)
{
	CalculateBoundary(hitData->GetBlobData(RED_COLOR));
	CalculateBoundary(hitData->GetBlobData(GREEN_COLOR));
	CalculateBoundary(hitData->GetBlobData(BLUE_COLOR));
	CalculateBoundary(hitData->GetBlobData(RB_COLORS));
	CalculateBoundary(hitData->GetBlobData(GB_COLORS));
}

void CHitFindingOperation::CalculateBoundary(vector<CBlobData *> *blobData)
{
	for (int i = 0; i < (int)blobData->size(); i++)
	{
		GetBlobBoundary((*blobData)[i]);
	}
}

bool CHitFindingOperation::ProcessOneRegion(CCTCParams *params, CRGNData *region, bool isZeissData)
{
	bool ret = false;
	FreeBlobList(region->GetBlobData(RED_COLOR));
	FreeBlobList(region->GetBlobData(GREEN_COLOR));
	FreeBlobList(region->GetBlobData(BLUE_COLOR));
	FreeBlobList(region->GetBlobData(RB_COLORS));
	FreeBlobList(region->GetBlobData(GB_COLORS));
	region->m_RedFrameMax = region->GetCPI(RED_COLOR) + region->GetContrast(RED_COLOR);
	region->m_GreenFrameMax = region->GetCPI(GREEN_COLOR) + region->GetContrast(GREEN_COLOR);
	region->m_BlueFrameMax = region->GetCPI(BLUE_COLOR) + region->GetContrast(BLUE_COLOR);
	vector<POINT> pts;
	GetBlueBlobs(region, &pts, false);
	bool firstRegion = true;
	vector<CBlobData *> list;
	vector<CRGNData *> cells;
	CRGNData *newRegion = NULL;
	for (int i = 0; i < (int)pts.size(); i++)
	{ 
		if (firstRegion)
		{
			bool found = FindCompositeBlobs(region, pts[i], &list);
			if (found)
			{
				ret = true;
				CalculateBoundary(region);
				CalculateCellAttributes(params, region, isZeissData);
				firstRegion = false;
				if ((int)list.size() > 0)
				{
					newRegion = new CRGNData(region);
					for (int j = 0; j < (int)list.size(); j++)
					{
						newRegion->GetBlobData(BLUE_COLOR)->push_back(list[j]);
						list[j] = NULL;
					}
					list.clear();
				}
				else
					break;
			}
		}
		else
		{
			bool found = FindCompositeBlobs(newRegion, pts[i], &list);
			if (found)
			{
				CalculateBoundary(newRegion);
				CalculateCellAttributes(params, newRegion, isZeissData);
				cells.push_back(newRegion);
				newRegion = NULL;
				if ((int)list.size() > 0)
				{
					newRegion = new CRGNData(region);
					for (int j = 0; j < (int)list.size(); j++)
					{
						newRegion->GetBlobData(BLUE_COLOR)->push_back(list[j]);
						list[j] = NULL;
					}
					list.clear();
				}
				else
					break;
			}
		}
	}
	if (newRegion != NULL)
		delete newRegion;
	if (cells.size() > 0)
	{
		int left, right, top, bottom;
		region->GetBoundingBox(&left, &top, &right, &bottom);
		int offsetX = abs(((left + right) / 2) - (region->GetWidth() / 2));
		int offsetY = abs(((top + bottom) / 2) - (region->GetHeight() / 2));
		for (int i = 0; i < (int)cells.size(); i++)
		{
			cells[i]->GetBoundingBox(&left, &top, &right, &bottom);
			int offsetX1 = abs(((left + right) / 2) - (region->GetWidth() / 2));
			int offsetY1 = abs(((top + bottom) / 2) - (region->GetHeight() / 2));
			if ((offsetX1 < offsetX) && (offsetY1 < offsetY))
			{
				offsetX = offsetX1;
				offsetY = offsetY1;
				region->CopyRegionData(cells[i]);
			}
		}
		FreeRGNList(&cells);
	}
	return ret;
}

void  CHitFindingOperation::SetFrameMax(CRGNData *region, vector<CRGNData *> *additionRegions)
{
	int redMax = 0;
	int greenMax = 0;
	int blueMax = 0;

	for (int i = 0; i < (int)region->GetBlobData(RED_COLOR)->size(); i++)
	{
		if ((*region->GetBlobData(RED_COLOR))[i]->m_Intensity > redMax)
			redMax = (*region->GetBlobData(RED_COLOR))[i]->m_Intensity;
	}
	for (int i = 0; i < (int)region->GetBlobData(GREEN_COLOR)->size(); i++)
	{
		if ((*region->GetBlobData(GREEN_COLOR))[i]->m_Intensity > greenMax)
			greenMax = (*region->GetBlobData(GREEN_COLOR))[i]->m_Intensity;
	}
	for (int i = 0; i < (int)region->GetBlobData(BLUE_COLOR)->size(); i++)
	{
		if ((*region->GetBlobData(BLUE_COLOR))[i]->m_Intensity > blueMax)
			blueMax = (*region->GetBlobData(BLUE_COLOR))[i]->m_Intensity;
	}

	if (additionRegions != NULL)
	{
		for (int j = 0; j < (int)additionRegions->size(); j++)
		{
			for (int i = 0; i < (int)(*additionRegions)[j]->GetBlobData(RED_COLOR)->size(); i++)
			{
				if ((*(*additionRegions)[j]->GetBlobData(RED_COLOR))[i]->m_Intensity > redMax)
					redMax = (*(*additionRegions)[j]->GetBlobData(RED_COLOR))[i]->m_Intensity;
			}
			for (int i = 0; i < (int)(*additionRegions)[j]->GetBlobData(GREEN_COLOR)->size(); i++)
			{
				if ((*(*additionRegions)[j]->GetBlobData(GREEN_COLOR))[i]->m_Intensity > greenMax)
					greenMax = (*(*additionRegions)[j]->GetBlobData(GREEN_COLOR))[i]->m_Intensity;
			}
			for (int i = 0; i < (int)(*additionRegions)[j]->GetBlobData(BLUE_COLOR)->size(); i++)
			{
				if ((*(*additionRegions)[j]->GetBlobData(BLUE_COLOR))[i]->m_Intensity > blueMax)
					blueMax = (*(*additionRegions)[j]->GetBlobData(BLUE_COLOR))[i]->m_Intensity;
			}
		}
	}

	region->m_RedFrameMax = redMax;
	region->m_GreenFrameMax = greenMax;
	region->m_BlueFrameMax = blueMax;
	if (additionRegions != NULL)
	{
		for (int j = 0; j < (int)additionRegions->size(); j++)
		{
			(*additionRegions)[j]->m_RedFrameMax = redMax;
			(*additionRegions)[j]->m_GreenFrameMax = greenMax;
			(*additionRegions)[j]->m_BlueFrameMax = blueMax;
		}
	}
}

int CHitFindingOperation::GetPixelCountSum(CRGNData *region, PIXEL_COLOR_TYPE color)
{
	int pixelCount = 0;
	
	for (int i = 0; i < (int)region->GetBlobData(color)->size(); i++)
		pixelCount += (int)(*region->GetBlobData(color))[i]->m_Pixels->size();
	
	return pixelCount;
}

void CHitFindingOperation::SetBoundingBox(CRGNData *region, vector<int> *map)
{
	int left = region->GetWidth();
	int right = 0;
	int top = region->GetHeight();
	int bottom = 0;

	for (int i = 0; i < (int)map->size(); i++)
	{
		int x = (*map)[i] % region->GetWidth();
		int y = (*map)[i] / region->GetWidth();
		if (x < left)
			left = x;
		if (x > right)
			right = x;
		if (y < top)
			top = y;
		if (y > bottom)
			bottom = y;
	}

	region->SetBoundingBox(left, top, right, bottom);
}

void CHitFindingOperation::CalculateCellAttributes(CCTCParams *params, CRGNData *region, bool IsZeissData)
{
	region->SetPixels(RED_COLOR, GetPixelCountSum(region, RED_COLOR));
	region->SetPixels(GREEN_COLOR, GetPixelCountSum(region, GREEN_COLOR));
	region->SetPixels(BLUE_COLOR, GetPixelCountSum(region, BLUE_COLOR));
	region->SetPixels(RB_COLORS, GetPixelCountSum(region, RB_COLORS));
	region->SetPixels(GB_COLORS, GetPixelCountSum(region, GB_COLORS));
	vector<int> *map;
	if (region->GetPixels(RB_COLORS) >= region->GetPixels(GB_COLORS))
	{
		map = (*region->GetBlobData(RB_COLORS))[0]->m_Pixels;
	}
	else
	{
		map = (*region->GetBlobData(GB_COLORS))[0]->m_Pixels;
	}
	SetBoundingBox(region, map);
	region->m_RedAverage = GetAverageIntensity(region->GetImage(RED_COLOR), map);
	region->m_GreenAverage = GetAverageIntensity(region->GetImage(GREEN_COLOR), map);
	region->m_BlueAverage = GetAverageIntensity(region->GetImage(BLUE_COLOR), map);
	UINT32 *hist = new UINT32[CPI_RANGE_LENGTH];
	memset(hist, 0, sizeof(UINT32) * CPI_RANGE_LENGTH);
	unsigned short *image = region->GetImage(RED_COLOR);
	for (int i = 0; i < PATCH_HEIGHT; i++)
	{
		for (int j = 0; j < PATCH_WIDTH; j++, image++)
		{
			int value = *image;
			if ((value >= CPI_RANGE_START) && (value < CPI_RANGE_END))
				hist[value - CPI_RANGE_START]++;
		}
	}
	UINT32 maxHistCount = 0;
	int maxHistIndex = 0;
	for (int i = 0; i < CPI_RANGE_LENGTH; i++)
	{
		if (hist[i] > maxHistCount)
		{
			maxHistCount = hist[i];
			maxHistIndex = i;
		}
	}
	region->SetCPI(RED_COLOR, maxHistIndex + CPI_RANGE_START);
	memset(hist, 0, sizeof(UINT32) * CPI_RANGE_LENGTH);
	image = region->GetImage(GREEN_COLOR);
	for (int i = 0; i < PATCH_HEIGHT; i++)
	{
		for (int j = 0; j < PATCH_WIDTH; j++, image++)
		{
			int value = *image;
			if ((value >= CPI_RANGE_START) && (value < CPI_RANGE_END))
				hist[value - CPI_RANGE_START]++;
		}
	}
	maxHistCount = 0;
	maxHistIndex = 0;
	for (int i = 0; i < CPI_RANGE_LENGTH; i++)
	{
		if (hist[i] > maxHistCount)
		{
			maxHistCount = hist[i];
			maxHistIndex = i;
		}
	}
	region->SetCPI(GREEN_COLOR, maxHistIndex + CPI_RANGE_START);
	delete hist;

	if (region->m_RedAverage > region->GetCPI(RED_COLOR))
		region->m_RedValue = region->m_RedAverage - region->GetCPI(RED_COLOR);
	else
		region->m_RedValue = 0;
	if (region->m_GreenAverage > region->GetCPI(GREEN_COLOR))
		region->m_GreenValue = region->m_GreenAverage - region->GetCPI(GREEN_COLOR);
	else
		region->m_GreenValue = 0;

	float nominator = (float) (params->m_GroupParams[(int)PARAM_NOMINATOR_R_COEF1 - (int)PARAM_FLOAT_FIRST] * region->m_RedValue
		+ params->m_GroupParams[(int)PARAM_NOMINATOR_G_COEF1 - (int)PARAM_FLOAT_FIRST] * region->m_GreenValue);
	float denominator = (float) (params->m_GroupParams[(int)PARAM_DENOMINATOR_R_COEF1 - (int)PARAM_FLOAT_FIRST] * region->m_RedValue
		+ params->m_GroupParams[(int)PARAM_DENOMINATOR_G_COEF1 - (int)PARAM_FLOAT_FIRST] * region->m_GreenValue + 1.0);

	if ((nominator > 0) && (denominator > 0))
		region->m_Ratio1 = ((double)nominator) / ((double)denominator);
	else if (nominator <= 0)
		region->m_Ratio1 = 0;
	else
	{
		region->m_Ratio1 = nominator;
	}

	if (region->m_Ratio1 <= (double)params->m_GroupParams[(int)PARAM_COEF1_THRESHOLD - (int)PARAM_FLOAT_FIRST])
	{
		region->SetColorCode(NONCTC);
	}
	else
	{
		region->SetColorCode(CTC);
	}
}

bool CHitFindingOperation::GetPatchImages(CRGNData *region, CSingleChannelTIFFData *redImage, CSingleChannelTIFFData *greenImage, CSingleChannelTIFFData *blueImage)
{
	bool ret = false;

	unsigned short *redImg = new unsigned short[sizeof(unsigned short) * PATCH_WIDTH * PATCH_HEIGHT];
	unsigned short *greenImg = new unsigned short[sizeof(unsigned short) * PATCH_WIDTH * PATCH_HEIGHT];
	unsigned short *blueImg = new unsigned short[sizeof(unsigned short) * PATCH_WIDTH * PATCH_HEIGHT];
	region->SetImages(redImg, greenImg, blueImg);
	int x0, y0;
	region->GetPosition(&x0, &y0);
	while (true)
	{
		ret = redImage->GetImageRegion(x0, y0, PATCH_WIDTH, PATCH_HEIGHT, region->GetImage(RED_COLOR));
		if (!ret)
			break;
		ret = greenImage->GetImageRegion(x0, y0, PATCH_WIDTH, PATCH_HEIGHT, region->GetImage(GREEN_COLOR));
		if (!ret)
			break;
		ret = blueImage->GetImageRegion(x0, y0, PATCH_WIDTH, PATCH_HEIGHT, region->GetImage(BLUE_COLOR));
		if (!ret)
			break;
	}
	return ret;
}

