﻿
// CTCViewerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CTCViewer.h"
#include "CTCViewerDlg.h"
#include "afxdialogex.h"
#include "string.h"
#include "SingleRegionImageData.h"
#include "VersionNumber.h"
#include <atlstr.h>
#include "ColorType.h"
#include "AddOneCTCDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define WM_MY_MESSAGE (WM_USER+1001)
#define WM_MY_MESSAGE2 (WM_USER+1002)

#define HIT_FILE_VERSION 4
#define MAX_MAGNIFICATION 10
#define MIN_MAGNIFICATION 1

// CCTCViewerDlg dialog
static bool toggleFlag = true;
static int mouseIndex = 0;

CCTCViewerDlg::CCTCViewerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCTCViewerDlg::IDD, pParent)
	, m_RGNFilename(_T(""))
	, m_TIFFFilename(_T(""))
	, m_FullPathTIFFFilename(_T(""))
	, m_SampleName(_T(""))
	, m_Status(_T(""))
	, m_CTCCount(0)
	, m_ConfirmedCTCNum(0)
	, m_ConfirmedNonCTCNum(0)
	, m_CTCNum(0)
	, m_Description(_T(""))
	, m_GreenFilename(_T(""))
	, m_BlueFilename(_T(""))
	, m_Comment(_T(""))
	, m_CommentFileName(_T(""))
	, m_ReviewerName(_T(""))
	, m_ReviewerNameInRGNFile(_T(""))
	, m_UsingHitImageData(false)
	, m_TwoThresholds(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_ROIRect.left = 100;
	m_ROIRect.right = 105;
	m_ROIRect.top = 100;
	m_ROIRect.bottom = 105;
	m_RegionX0 = 0;
	m_RegionY0 = 0;
	m_RegionWidth = 0;
	m_RegionHeight = 0;
	m_RedRegionImage = NULL;
	m_GreenRegionImage = NULL;
	m_BlueRegionImage = NULL;
	Leica_Red_Prefix = _T("");
	Leica_Green_Prefix = _T("");
	Leica_Blue_Prefix = _T("");
	Zeiss_Red_Postfix = _T("");
	Zeiss_Green_Postfix = _T("");
	Zeiss_Blue_Postfix = _T("");
}

void CCTCViewerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IMAGEDISPLAY, m_ImageDisplay);
	DDX_Text(pDX, IDC_RGNFILENAME, m_RGNFilename);
	DDX_Text(pDX, IDC_TIFFILENAME, m_TIFFFilename);
	DDX_Text(pDX, IDC_STATUS, m_Status);
	DDX_Control(pDX, IDC_BLUE, m_Blue);
	DDX_Control(pDX, IDC_GREEN, m_Green);
	DDX_Control(pDX, IDC_RED, m_Red);
	DDX_Text(pDX, IDC_CTCCount, m_CTCCount);
	DDX_Control(pDX, IDC_COLORPAD, m_ColorPadDisplay);
	DDX_Control(pDX, IDC_NONCK, m_NonCKRadio);
	DDX_Control(pDX, IDC_WBCS, m_WBCsRadio);
	DDX_Control(pDX, IDC_CONFIRMEDCTC, m_ConfirmedCTCRadio);
	DDX_Control(pDX, IDC_C_LIGHT_BLUE, m_CLightBlue);
	DDX_Control(pDX, IDC_C_LIGHT_BLUE2, m_CLightBlue2);
	DDX_Control(pDX, IDC_C_PINK, m_CPink);
	DDX_Control(pDX, IDC_C_RED, m_CRed);
	DDX_Text(pDX, IDC_CONFIRMCTCNUM, m_ConfirmedCTCNum);
	DDX_Text(pDX, IDC_CONFIRMNONCTCNUM, m_ConfirmedNonCTCNum);
	DDX_Text(pDX, IDC_CTCNUM, m_CTCNum);
	DDX_Control(pDX, IDC_REGIONNUM, m_CTCIndex);
	DDX_Control(pDX, IDC_BLUESLIDE, m_BlueSlider);
	DDX_Control(pDX, IDC_GREENSLID, m_GreenSlider);
	DDX_Control(pDX, IDC_REDSLIDE, m_RedSlider);
	DDX_Text(pDX, IDC_GREENCONTRAST, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
	DDX_Text(pDX, IDC_GREENCUTOFF, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
	DDX_Text(pDX, IDC_REDCONTRAST, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
	DDX_Text(pDX, IDC_REDCUTOFF, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
	DDX_Text(pDX, IDC_BLUECONTRAST, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
	DDX_Text(pDX, IDC_BLUECUTOFF, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
	DDX_Control(pDX, IDC_USEKEY, m_UseKeyStroke);
	DDX_Text(pDX, IDC_DESCRIPTION, m_Description);
	DDX_Control(pDX, IDC_USEBOX, m_UseIntensityBox);
	DDX_Control(pDX, IDC_MAG1, m_Mag1);
	DDX_Text(pDX, IDC_GREENFILENAME, m_GreenFilename);
	DDX_Text(pDX, IDC_BLUEFILENAME, m_BlueFilename);
	DDX_Control(pDX, IDC_CELLSCORELIST, m_CellScoreList);
	DDX_Text(pDX, IDC_BREDTHR, m_CTCParams.m_CTCParams[(int)PARAM_RED_THRESHOLD]);
	DDX_Text(pDX, IDC_MAXBLOBPIXELCOUNT, m_CTCParams.m_CTCParams[(int)PARAM_MAX_BLOBPIXELCOUNT]);
	DDV_MinMaxInt(pDX, m_CTCParams.m_CTCParams[(int)PARAM_MAX_BLOBPIXELCOUNT], 20, 2500);
	DDX_Text(pDX, IDC_MINBLOBPIXELCOUNT, m_CTCParams.m_CTCParams[(int)PARAM_MIN_BLOBPIXELCOUNT]);
	DDV_MinMaxInt(pDX, m_CTCParams.m_CTCParams[(int)PARAM_MIN_BLOBPIXELCOUNT], 10, 800);
	DDX_Control(pDX, IDC_LOADONECHANNEL, m_LoadOneChannel);
	DDX_Text(pDX, IDC_COMMENT, m_Comment);
	DDX_Control(pDX, IDC_SHOWBOUNDARY, m_ShowBoundary);
	DDX_Control(pDX, IDC_FORCTCONLY, m_RankPossibleOnly);
	DDX_Control(pDX, IDC_FORCTC2ONLY, m_RankConfirmedOnly);
	DDX_Control(pDX, IDC_FORALLREGIONS, m_RankAllRegions);
	DDX_Control(pDX, IDC_TWOTYPECTCS, m_TwoTypeCTCs);
	DDX_Control(pDX, IDC_NONCTCONLY, m_NonCTCOnly);
	DDX_Text(pDX, IDC_WBCGREENREL, m_CTCParams.m_WBCGreenRelative);
	DDX_Text(pDX, IDC_RDRMGTHRESHOLD, m_TwoThresholds);
	DDX_Text(pDX, IDC_WBCGREENMARKUP, m_CTCParams.m_CTCParams[(int)PARAM_WBCGREENMARKUP]);
	DDX_Text(pDX, IDC_GREENCPI, m_GreenIntensity);
	DDX_Control(pDX, IDC_ZOOMIN, m_ResetZoomOut);
	DDX_Text(pDX, IDC_REVIEWER, m_ReviewerName);
}

BEGIN_MESSAGE_MAP(CCTCViewerDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCANCEL, &CCTCViewerDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BLUE, &CCTCViewerDlg::OnBnClickedBlue)
	ON_BN_CLICKED(IDC_RED, &CCTCViewerDlg::OnBnClickedRed)
	ON_BN_CLICKED(IDC_GREEN, &CCTCViewerDlg::OnBnClickedGreen)
	ON_BN_CLICKED(IDC_NEXT, &CCTCViewerDlg::OnBnClickedNext)
	ON_BN_CLICKED(IDC_PREV, &CCTCViewerDlg::OnBnClickedPrev)
	ON_BN_CLICKED(IDC_SAVERGN, &CCTCViewerDlg::OnBnClickedSavergn)
	ON_BN_CLICKED(IDC_CONFIRMEDCTC, &CCTCViewerDlg::OnBnClickedConfirmedctc)
	ON_BN_CLICKED(IDC_SELECT, &CCTCViewerDlg::OnBnClickedSelect)
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_SAVEIMAGE, &CCTCViewerDlg::OnBnClickedSaveimage)
	ON_BN_CLICKED(IDC_REDRESET, &CCTCViewerDlg::OnBnClickedRedreset)
	ON_BN_CLICKED(IDC_GREENRESET, &CCTCViewerDlg::OnBnClickedGreenreset)
	ON_BN_CLICKED(IDC_BLUERESET, &CCTCViewerDlg::OnBnClickedBluereset)
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_REDFILE, &CCTCViewerDlg::OnBnClickedRedfile)
	ON_WM_RBUTTONDOWN()
	ON_BN_CLICKED(IDC_RESETCONTRAST, &CCTCViewerDlg::OnBnClickedResetcontrast)
	ON_BN_CLICKED(IDC_USEBOX, &CCTCViewerDlg::OnBnClickedUsebox)
	ON_BN_CLICKED(IDC_RELOAD, &CCTCViewerDlg::OnBnClickedReload)
	ON_BN_CLICKED(IDC_SAVEDATA, &CCTCViewerDlg::OnBnClickedSavedata)
	ON_MESSAGE(WM_MY_MESSAGE, OnMyMessage)
	ON_MESSAGE(WM_MY_MESSAGE2, OnMyMessage2)
	ON_BN_CLICKED(IDC_OPENRGN, &CCTCViewerDlg::OnBnClickedOpenrgn)
	ON_BN_CLICKED(IDC_ADDONE, &CCTCViewerDlg::OnBnClickedAddone)
	ON_BN_CLICKED(IDC_FORCTCONLY, &CCTCViewerDlg::OnBnClickedForctconly)
	ON_BN_CLICKED(IDC_FORCTC2ONLY, &CCTCViewerDlg::OnBnClickedForctc2only)
	ON_BN_CLICKED(IDC_TWOTYPECTCS, &CCTCViewerDlg::OnBnClickedTwotypectcs)
	ON_BN_CLICKED(IDC_NONCTCONLY, &CCTCViewerDlg::OnBnClickedNonctconly)
	ON_BN_CLICKED(IDC_FORALLREGIONS, &CCTCViewerDlg::OnBnClickedForallregions)
	ON_BN_CLICKED(IDC_ZOOMIN, &CCTCViewerDlg::OnBnClickedZoomin)
	ON_BN_CLICKED(IDC_DELETEONE, &CCTCViewerDlg::OnBnClickedDeleteone)
	ON_BN_CLICKED(IDC_NONCK, &CCTCViewerDlg::OnBnClickedNonck)
	ON_BN_CLICKED(IDC_WBCS, &CCTCViewerDlg::OnBnClickedWbcs)
END_MESSAGE_MAP()


// CCTCViewerDlg message handlers

BOOL CCTCViewerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	CString version;
	version.Format(_T("CellRegionReviewer(%s)"), CELLMAPPLUS_VERSION);
	m_Log.NewLog(version);
	SetWindowText(version);
	m_ZeissData = false;
	version.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
	m_CTCParams.LoadCTCParams(version, &m_TwoThresholds);
	LoadDefaultSettings();
	m_RedIntensity = 0;
	m_GreenIntensity = 0;
	m_BlueIntensity = 0;
	m_RedTIFFData = new CSingleChannelTIFFData();
	m_GreenTIFFData = new CSingleChannelTIFFData();
	m_BlueTIFFData = new CSingleChannelTIFFData();
	m_Blue.SetCheck(TRUE);
	m_Green.SetCheck(TRUE);
	m_Red.SetCheck(TRUE);
	m_Status = "Please load Red Channel TIFF Image File";
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	m_ImageWidth = 0;
	m_ImageHeight = 0;
	m_DisplayedRgnIndex = 0;
	m_CTCIndex.SetWindowTextW(_T("0"));
	int nWidth = 40;
	int nHeight = 20;
	for (int i = 0; i < 4; i++)
	{
		m_ColorImage[i].Create(nWidth, -nHeight, 24);
		UINT32 colorCode = 0;
		switch (i)
		{
		case 0:
			colorCode = CTC;
			break;
		case 1:
			colorCode = CTC2;
			break;
		case 2:
			colorCode = NONCTC;
			break;
		case 3:
			colorCode = NONCTC;
			break;
		}
		
		BYTE *pCursor = (BYTE*)m_ColorImage[i].GetBits();
		int nStride = m_ColorImage[i].GetPitch() - (nWidth * 3);
		for (int y = 0; y<nHeight; y++)
		{
			for (int x = 0; x<nWidth; x++)
			{
				*pCursor++ = (BYTE)((colorCode >> 16) & 0xFF);
				*pCursor++ = (BYTE)((colorCode >> 8) & 0xFF);
				*pCursor++ = (BYTE)(colorCode & 0xFF);
			}
			if (nStride > 0)
				pCursor += nStride;
		}
	}
	m_BlueSlider.SetRange(10, 250, TRUE);
	m_GreenSlider.SetRange(10, 250, TRUE);
	m_RedSlider.SetRange(10, 250, TRUE);
	m_Mag1.SetRange(MIN_MAGNIFICATION, MAX_MAGNIFICATION, TRUE);
	ResetSlider();
	ResetMag1();
	m_RegionX0 = 0;
	m_RegionY0 = 0;
	m_RegionWidth = 100;
	m_RegionHeight = 100;
	ResetBoxPos();
	m_ShowBoundary.SetCheck(BST_UNCHECKED);
	m_RankAllRegions.SetCheck(BST_CHECKED);
	int nSize[] = { 150, 80 };
	LV_COLUMN nListColumn;
	for (int i = 0; i < 2; i++)
	{
		nListColumn.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
		nListColumn.fmt = LVCFMT_LEFT;
		nListColumn.cx = nSize[i];
		nListColumn.iSubItem = 0;
		if (i == 0)
			nListColumn.pszText = _T("Name");
		else if (i == 1)
			nListColumn.pszText = _T("Value");

		m_CellScoreList.InsertColumn(i, &nListColumn);
	}
	m_CellScoreList.SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT);
	m_LoadOneChannel.SetCheck(BST_UNCHECKED);
	m_HitFinder.m_Log = &m_Log;
	m_UseKeyStroke.SetCheck(BST_CHECKED);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CCTCViewerDlg::LoadDefaultSettings()
{
	CStdioFile theFile;
	if (theFile.Open(_T(".\\DefaultSettings.txt"), CFile::modeRead | CFile::typeText))
	{
		CString textline;
		while (theFile.ReadString(textline))
		{
			if (textline.Find(_T("Leica_Red_Prefix=")) > -1)
			{
				CString tag = _T("Leica_Red_Prefix=");
				Leica_Red_Prefix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Leica_Green_Prefix=")) > -1)
			{
				CString tag = _T("Leica_Green_Prefix=");
				Leica_Green_Prefix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Leica_Blue_Prefix=")) > -1)
			{
				CString tag = _T("Leica_Blue_Prefix=");
				Leica_Blue_Prefix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Zeiss_Red_Postfix=")) > -1)
			{
				CString tag = _T("Zeiss_Red_Postfix=");
				Zeiss_Red_Postfix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Zeiss_Green_Postfix=")) > -1)
			{
				CString tag = _T("Zeiss_Green_Postfix=");
				Zeiss_Green_Postfix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Zeiss_Blue_Postfix=")) > -1)
			{
				CString tag = _T("Zeiss_Blue_Postfix=");
				Zeiss_Blue_Postfix = textline.Mid(tag.GetLength());
			}
		}
		theFile.Close();
	}
}


// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCTCViewerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		CPaintDC dc0(&m_CRed);
		CPaintDC dc1(&m_CPink);
		CPaintDC dc2(&m_CLightBlue);
		CPaintDC dc3(&m_CLightBlue2);
		CRect rect1;
		m_CRed.GetClientRect(&rect1);
		dc0.SetStretchBltMode(HALFTONE);
		m_ColorImage[0].StretchBlt(dc0.m_hDC, rect1);
		m_CPink.GetClientRect(&rect1);
		dc1.SetStretchBltMode(HALFTONE);
		m_ColorImage[1].StretchBlt(dc1.m_hDC, rect1);
		m_CLightBlue.GetClientRect(&rect1);
		dc2.SetStretchBltMode(HALFTONE);
		m_ColorImage[2].StretchBlt(dc2.m_hDC, rect1);
		m_CLightBlue2.GetClientRect(&rect1);
		dc3.SetStretchBltMode(HALFTONE);
		m_ColorImage[3].StretchBlt(dc3.m_hDC, rect1);
		if (m_Image != NULL)
		{
			CPaintDC dc(&m_ImageDisplay);
			CRect rect;
			m_ImageDisplay.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_Image.StretchBlt(dc.m_hDC, rect);
		}
		if (m_Pad != NULL)
		{
			CPaintDC dc(&m_ColorPadDisplay);
			CRect rect;
			m_ColorPadDisplay.GetClientRect(&rect);
			dc.SetStretchBltMode(HALFTONE);
			m_Pad.StretchBlt(dc.m_hDC, rect);
		}
		if ((m_UseIntensityBox.GetCheck() == BST_CHECKED) && (m_DisplayedRgnIndex > 0) && 
			((m_DisplayedRgnIndex - 1) < (int) m_RGNDataArray.size()) && (toggleFlag) && (mouseIndex == 1))
		{
			CDC* pDC = m_ImageDisplay.GetDC();
			DrawROIWindow(pDC, m_ROIRect);
			DisplayBoxDescription();
		}
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCTCViewerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CCTCViewerDlg::OnBnClickedCancel()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	FreeRegionImageData();
	FreeRGNData(&m_RGNDataArray);
	delete m_RedTIFFData;
	delete m_GreenTIFFData;
	delete m_BlueTIFFData;
	m_CellScoreList.DeleteAllItems();
	m_Comments.clear();
	CDialogEx::OnCancel();
}

bool CCTCViewerDlg::ReadRgnfile()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	bool ret = false;
	m_RedIntensity = m_RedTIFFData->GetCPI();
	m_GreenIntensity = m_GreenTIFFData->GetCPI();
	m_BlueIntensity = m_BlueTIFFData->GetCPI();
	CString message;
	CFileDialog dlg(TRUE,    // open
			NULL,    // no default extension
			NULL,    // no initial file name
			OFN_FILEMUSTEXIST
			| OFN_HIDEREADONLY,
			_T("RGN files (*.rgn, *.cz)|*.rgn; *.cz"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		m_CommentFileName = filename;
		WCHAR *char1 = _T(".");
		int commentFileIndex = m_CommentFileName.ReverseFind(*char1);
		m_CommentFileName = m_CommentFileName.Mid(0, commentFileIndex + 1) + _T("txt");
		ReadComments(m_CommentFileName);
		m_RGNFilename = dlg.GetFileName();
		if (LoadRegionData(filename))
		{
			LoadCheckListData(m_CommentFileName);
			message.Format(_T("Region File %s loaded successfully, #Hits=%d"), filename, m_CTCCount);
			m_Log.Message(message);
			ret = true;
		}	
		else
		{
			m_Status.Format(_T("Failed to load RGN File %s"), m_RGNFilename);
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		}
	}
	else
	{
		m_Status = _T("Failed to provide RGN File Name");
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
	return true;
}

BOOL CCTCViewerDlg::LoadRegionData(CString filename)
{
	BOOL ret = FALSE;
	int failureLocation = 0;
	CFile theFile;
	CString message;
	message.Format(_T("Start to Load %s"), filename);
	m_Log.Message(message);
	UINT32 color = 0;
	int index = 0;

	try
	{
		if (theFile.Open(filename, CFile::modeRead))
		{
			failureLocation = 1;
			BYTE* buf = new BYTE[512];
			int count = 0;
			int offset = 0;
			char *ptr = NULL;
			char *ptr1 = NULL;
			int x0 = 0;
			int y0 = 0;

			while (TRUE)
			{
				failureLocation = 2;
				count = theFile.Read(&buf[offset], 1);
				if (count == 1)
				{
					failureLocation = 3;
					if ((buf[offset] == '\0') || (buf[offset] == '\n') || (buf[offset] == '\r'))
					{
						failureLocation = 4;
						if (offset > 0)
						{
							failureLocation = 5;
							if (filename.Find(_T(".rgn"), 0) > 0)
							{
								ptr = strchr((char *)buf, ',');
								if (ptr != NULL)
								{
									failureLocation = 6;
									ptr = strstr(ptr, " 1 ");
									if (ptr != NULL)
									{
										ptr += 3;
										ptr1 = strchr(ptr, ',');
										if (ptr1 != NULL)
										{
											failureLocation = 7;
											*ptr1 = '\0';
											color = (UINT32)atoi(ptr);
											ptr1++;
											ptr = strstr(ptr1, " 2 ");
											if (ptr != NULL)
											{
												failureLocation = 8;
												ptr += 3;
												ptr1 = strchr(ptr, ' ');
												if (ptr1 != NULL)
												{
													failureLocation = 9;
													*ptr1 = '\0';
													x0 = atoi(ptr);
													ptr1++;
													ptr = strchr(ptr1, ',');
													if (ptr != NULL)
													{
														failureLocation = 10;
														*ptr = '\0';
														y0 = atoi(ptr1);
														CRGNData* data = new CRGNData(x0, y0, PATCH_WIDTH, PATCH_HEIGHT);
														data->SetColorCode(color);
														data->SetCPI(RED_COLOR, m_RedIntensity);
														data->SetCPI(GREEN_COLOR, m_GreenIntensity);
														data->SetCPI(BLUE_COLOR, m_BlueIntensity);
														data->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
														data->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
														data->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
														data->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
														data->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
														data->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
														data->SetThreshold(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_THRESHOLD]);
														data->SetThreshold(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_THRESHOLD]);
														data->SetThreshold(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_THRESHOLD]);
														m_RGNDataArray.push_back(data);
														ret = TRUE;
														offset = 0;
														failureLocation = 11;
													}
												}
											}
										}
									}
								}
							}
							else
							{
								failureLocation = 8;
								int type = 0;
								buf[offset + 1] = '\0';
								ptr1 = NULL;
								ptr = strstr((char *)buf, "<Left>");
								if (ptr != NULL)
									type = 1;
								else
								{
									ptr = strstr((char *)buf, "<Top>");
									if (ptr != NULL)
										type = 2;
									else
									{
										failureLocation = 9;
										type = 0;
										if (buf[offset] == '\0')
											break;
										else
										{
											offset = 0;
											continue;
										}
									}
								}
								if (type == 1)
								{
									failureLocation = 10;
									ptr += 6;
									ptr1 = strchr(ptr, '<');
									*ptr1 = '\0';
									x0 = (int)atof(ptr);
									offset = 0;
									continue;
								}
								else if (type == 2)
								{
									failureLocation = 11;
									ptr += 5;
									ptr1 = strchr(ptr, '<');
									*ptr1 = '\0';
									y0 = (int)atof(ptr);
									color = CTC;
									CRGNData* data = new CRGNData(x0, y0, PATCH_WIDTH, PATCH_HEIGHT);
									data->SetColorCode(color);
									data->SetCPI(RED_COLOR, m_RedIntensity);
									data->SetCPI(GREEN_COLOR, m_GreenIntensity);
									data->SetCPI(BLUE_COLOR, m_BlueIntensity);
									data->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
									data->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
									data->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
									data->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
									data->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
									data->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
									data->SetThreshold(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_THRESHOLD]);
									data->SetThreshold(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_THRESHOLD]);
									data->SetThreshold(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_THRESHOLD]);
									m_RGNDataArray.push_back(data);
									ret = TRUE;
									offset = 0;
									continue;
								}
							}
						}
						else
						{
							if (buf[offset] == '\0')
							{
								failureLocation = 12;
								break;
							}
							else
							{
								failureLocation = 13;
								offset = 0;
								continue;
							}
						}
					}
					else
					{
						failureLocation = 14;
						offset++;
					}
				}
				else
				{
					failureLocation = 15;
					break;
				}
			}

			theFile.Close();
			delete[] buf;
			buf = NULL;
		}
	}
	catch (CException *e)
	{
		TCHAR errCause[255];
		e->GetErrorMessage(errCause, 255);
		CString msg;
		msg.Format(_T("Cought Exception %s, Failre Location = %d"), errCause, failureLocation);
		m_Log.Message(msg);
		ret = FALSE;
	}
	catch (...)
	{
		CString msg;
		msg.Format(_T("Cought Exception, Failre Location = %d"), failureLocation);
		m_Log.Message(msg);
		ret = FALSE;
	}
	return ret;
}

// Copies the content of a byte buffer to a MFC image with respect to the image's alignment
void CCTCViewerDlg::CopyToRGBImage(unsigned short *pBlueBuffer, unsigned short *pGreenBuffer, unsigned short *pRedBuffer, 
	CImage *pOutImage, CRGNData *hitData)
{
	if (NULL != *pOutImage)
	{
		BYTE *pCursor = (BYTE*)pOutImage->GetBits();
		int nHeight = m_ImageHeight;
		int nWidth = m_ImageWidth;
		int BlueMax = m_BlueSlider.GetPos();
		int GreenMax = m_GreenSlider.GetPos();
		int RedMax = m_RedSlider.GetPos();
		
		int nStride = pOutImage->GetPitch() - (nWidth * 3);

		for (int y = 0; y<nHeight; y++)
		{
			for (int x = 0; x<nWidth; x++)
			{
				if (pBlueBuffer != NULL)
					*pCursor++ = GetContrastEnhancedByte(*pBlueBuffer++, BlueMax, BLUE_COLOR, hitData->GetCPI(BLUE_COLOR));
				else
					*pCursor++ = (BYTE)0;
				if (pGreenBuffer != NULL)
					*pCursor++ = GetContrastEnhancedByte(*pGreenBuffer++, GreenMax, GREEN_COLOR, hitData->GetCPI(GREEN_COLOR));
				else
					*pCursor++ = (BYTE)0;
				if (pRedBuffer != NULL)
					*pCursor++ = GetContrastEnhancedByte(*pRedBuffer++, RedMax, RED_COLOR, hitData->GetCPI(RED_COLOR));
				else
					*pCursor++ = (BYTE)0;
			}
			if (nStride > 0)
				pCursor += nStride;
		}

		if ((m_ShowBoundary.GetCheck() == BST_CHECKED) && (hitData != NULL) && (m_Mag1.GetPos() == MAX_MAGNIFICATION))
		{
			vector<CBlobData *> *redBlobs = hitData->GetBlobData(RED_COLOR);
			vector<CBlobData *> *greenBlobs = hitData->GetBlobData(GREEN_COLOR);
			vector<CBlobData *> *blueBlobs = hitData->GetBlobData(BLUE_COLOR);
			vector<CBlobData *> *gbBlobs = hitData->GetBlobData(GB_COLORS);
			vector<CBlobData *> *rbBlobs = hitData->GetBlobData(RB_COLORS);

			if ((pRedBuffer != NULL) && (pGreenBuffer == NULL) && (pBlueBuffer == NULL) && (redBlobs->size() > 0))
			{
				pCursor = (BYTE*)pOutImage->GetBits();
				int pitch = pOutImage->GetPitch();
				for (int i = 0; i < (int)redBlobs->size(); i++)
				{
					for (int j = 0; j < (int)(*redBlobs)[i]->m_Boundary->size(); j++)
					{
						unsigned int pos = (*(*redBlobs)[i]->m_Boundary)[j];
						int x0 = pos % (*redBlobs)[i]->m_Width;
						int y0 = pos / (*redBlobs)[i]->m_Width;
						pCursor[pitch * y0 + 3 * x0] = (BYTE)255;
						pCursor[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
						pCursor[pitch * y0 + 3 * x0 + 2] = (BYTE)255;
					}
				}
			}

			if ((pGreenBuffer != NULL) && (pRedBuffer == NULL) && (pBlueBuffer == NULL) && (greenBlobs->size() > 0))
			{
				pCursor = (BYTE*)pOutImage->GetBits();
				int pitch = pOutImage->GetPitch();
				for (int i = 0; i < (int)greenBlobs->size(); i++)
				{
					for (int j = 0; j < (int)(*greenBlobs)[i]->m_Boundary->size(); j++)
					{
						unsigned int pos = (*(*greenBlobs)[i]->m_Boundary)[j];
						int x0 = pos % (*greenBlobs)[i]->m_Width;
						int y0 = pos / (*greenBlobs)[i]->m_Width;
						pCursor[pitch * y0 + 3 * x0] = (BYTE)255;
						pCursor[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
						pCursor[pitch * y0 + 3 * x0 + 2] = (BYTE)255;
					}
				}
			}

			if ((pBlueBuffer != NULL) && (pRedBuffer == NULL) && (pGreenBuffer == NULL) && (blueBlobs->size() > 0))
			{
				pCursor = (BYTE*)pOutImage->GetBits();
				int pitch = pOutImage->GetPitch();
				for (int i = 0; i < (int)blueBlobs->size(); i++)
				{
					for (int j = 0; j < (int)(*blueBlobs)[i]->m_Boundary->size(); j++)
					{
						unsigned int pos = (*(*blueBlobs)[i]->m_Boundary)[j];
						int x0 = pos % (*blueBlobs)[i]->m_Width;
						int y0 = pos / (*blueBlobs)[i]->m_Width;
						pCursor[pitch * y0 + 3 * x0] = (BYTE)255;
						pCursor[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
						pCursor[pitch * y0 + 3 * x0 + 2] = (BYTE)255;
					}
				}
			}

			if ((pBlueBuffer != NULL) && (pRedBuffer != NULL) && (pGreenBuffer == NULL) && (rbBlobs->size() > 0))
			{
				pCursor = (BYTE*)pOutImage->GetBits();
				int pitch = pOutImage->GetPitch();
				for (int i = 0; i < (int)rbBlobs->size(); i++)
				{
					for (int j = 0; j < (int)(*rbBlobs)[i]->m_Boundary->size(); j++)
					{
						unsigned int pos = (*(*rbBlobs)[i]->m_Boundary)[j];
						int x0 = pos % (*rbBlobs)[i]->m_Width;
						int y0 = pos / (*rbBlobs)[i]->m_Width;
						pCursor[pitch * y0 + 3 * x0] = (BYTE)255;
						pCursor[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
						pCursor[pitch * y0 + 3 * x0 + 2] = (BYTE)255;
					}
				}
			}

			if ((pBlueBuffer != NULL) && (pGreenBuffer != NULL) && (pRedBuffer == NULL) && (gbBlobs->size() > 0))
			{
				pCursor = (BYTE*)pOutImage->GetBits();
				int pitch = pOutImage->GetPitch();
				for (int i = 0; i < (int)gbBlobs->size(); i++)
				{
					for (int j = 0; j < (int)(*gbBlobs)[i]->m_Boundary->size(); j++)
					{
						unsigned int pos = (*(*gbBlobs)[i]->m_Boundary)[j];
						int x0 = pos % (*gbBlobs)[i]->m_Width;
						int y0 = pos / (*gbBlobs)[i]->m_Width;
						pCursor[pitch * y0 + 3 * x0] = (BYTE)255;
						pCursor[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
						pCursor[pitch * y0 + 3 * x0 + 2] = (BYTE)255;
					}
				}
			}

			if ((pBlueBuffer != NULL) && (pGreenBuffer != NULL) && (pRedBuffer != NULL))
			{
				CBlobData *blob = NULL;
				if ((rbBlobs->size() > 0) && (gbBlobs->size() == 0))
				{
					blob = (*rbBlobs)[0];
				}
				else if ((gbBlobs->size() > 0) && (rbBlobs->size() == 0))
				{
					blob = (*gbBlobs)[0];
				}
				else if ((gbBlobs->size() > 0) && (rbBlobs->size() > 0))
				{
					CBlobData *rbBlob = (*rbBlobs)[0];
					CBlobData *gbBlob = (*gbBlobs)[0];
					if (rbBlob->m_Pixels->size() > gbBlob->m_Pixels->size())
						blob = rbBlob;
					else
						blob = gbBlob;
				}
				if (blob != NULL)
				{
					pCursor = (BYTE*)pOutImage->GetBits();
					int pitch = pOutImage->GetPitch();
					for (int j = 0; j < (int)blob->m_Boundary->size(); j++)
					{
						unsigned int pos = (*blob->m_Boundary)[j];
						int x0 = pos % blob->m_Width;
						int y0 = pos / blob->m_Width;
						pCursor[pitch * y0 + 3 * x0] = (BYTE)255;
						pCursor[pitch * y0 + 3 * x0 + 1] = (BYTE)255;
						pCursor[pitch * y0 + 3 * x0 + 2] = (BYTE)255;
					}
				}
			}
		}
	}
}

void CCTCViewerDlg::ClickColor()
{
	// TODO:  在此加入控制項告知處理常式程式碼	
	unsigned short *blue = m_BlueRegionImage;
	unsigned short *green = m_GreenRegionImage;
	unsigned short *red = m_RedRegionImage;
	if (m_Image != NULL)
	{
		m_Image.Destroy();
		m_ImageWidth = 0;
		m_ImageHeight = 0;
	}
	m_ImageWidth = m_RegionWidth;
	m_ImageHeight = m_RegionHeight;
	m_Image.Create(m_ImageWidth, -m_ImageHeight, 24);

	CopyToRGBImage((m_Blue.GetCheck() ? blue : NULL), (m_Green.GetCheck() ? green : NULL), (m_Red.GetCheck() ? red : NULL), &m_Image,
		m_RGNDataArray[m_DisplayedRgnIndex-1]);

	RECT rect1;
	m_ImageDisplay.GetWindowRect(&rect1);
	ScreenToClient(&rect1);
	InvalidateRect(&rect1, false);
	m_Status.Format(_T("Displayed ROI Image of Rgn No.%d, UpLeftCorner: X=%d,Y=%d"), m_DisplayedRgnIndex, m_RegionX0, m_RegionY0);
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
}


void CCTCViewerDlg::OnBnClickedRed()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);
	ClickColor();
	EnableButtons(TRUE);
}

void CCTCViewerDlg::OnBnClickedGreen()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);
	ClickColor();
	EnableButtons(TRUE);
}

void CCTCViewerDlg::OnBnClickedBlue()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);
	ClickColor();
	EnableButtons(TRUE);
}

bool CCTCViewerDlg::GetNextIndex()
{
	bool ret = false;
	for (int i = m_DisplayedRgnIndex; i < m_CTCCount; i++)
	{
		CRGNData *ptr = m_RGNDataArray[i];
		if ((m_RankPossibleOnly.GetCheck() == BST_CHECKED) && (ptr->GetColorCode() == CTC))
		{
			ret = true;
			m_DisplayedRgnIndex = i + 1;
			break;
		}
		else if ((m_RankConfirmedOnly.GetCheck() == BST_CHECKED) && (ptr->GetColorCode() == CTC2))
		{
			ret = true;
			m_DisplayedRgnIndex = i + 1;
			break;
		}
		else if ((m_TwoTypeCTCs.GetCheck() == BST_CHECKED) && 
			((ptr->GetColorCode() == CTC) || (ptr->GetColorCode() == CTC2)))
		{
			ret = true;
			m_DisplayedRgnIndex = i + 1;
			break;
		}
		else if ((m_NonCTCOnly.GetCheck() == BST_CHECKED) && (ptr->GetColorCode() == NONCTC))
		{
			ret = true;
			m_DisplayedRgnIndex = i + 1;
			break;
		}
		else if (m_RankAllRegions.GetCheck() == BST_CHECKED)
		{
			ret = true;
			m_DisplayedRgnIndex = i + 1;
			break;
		}
	}
	return ret;
}

bool CCTCViewerDlg::GetPrevIndex()
{
	bool ret = false;
	if (m_DisplayedRgnIndex < 2)  
		return ret;

	for (int i = m_DisplayedRgnIndex - 2; i >= 0; i--)
	{
		CRGNData *ptr = m_RGNDataArray[i];
		if ((m_RankPossibleOnly.GetCheck() == BST_CHECKED) && (ptr->GetColorCode() == CTC))
		{
			ret = true;
			m_DisplayedRgnIndex = i + 1;
			break;
		}
		else if ((m_RankConfirmedOnly.GetCheck() == BST_CHECKED) && (ptr->GetColorCode() == CTC2))
		{
			ret = true;
			m_DisplayedRgnIndex = i + 1;
			break;
		}
		else if ((m_TwoTypeCTCs.GetCheck() == BST_CHECKED) &&
			((ptr->GetColorCode() == CTC) || (ptr->GetColorCode() == CTC)))
		{
			ret = true;
			m_DisplayedRgnIndex = i + 1;
			break;
		}
		else if ((m_NonCTCOnly.GetCheck() == BST_CHECKED) && (ptr->GetColorCode() == NONCTC))
		{
			ret = true;
			m_DisplayedRgnIndex = i + 1;
			break;
		}
		else if (m_RankAllRegions.GetCheck() == BST_CHECKED)
		{
			ret = true;
			m_DisplayedRgnIndex = i + 1;
			break;
		}
	}
	return ret;
}

void CCTCViewerDlg::OnBnClickedNext()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);
	bool ret = GetNextIndex();
	if (!ret)
	{
		AfxMessageBox(_T("Reached the end of Region List"));
	}
	else
	{
		CString indexStr;
		indexStr.Format(_T("%d"), m_DisplayedRgnIndex);
		m_CTCIndex.SetWindowTextW(indexStr);
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		ResetMag1();
		LoadRegionImageFile(m_DisplayedRgnIndex - 1, m_Mag1.GetPos());
	}
	EnableButtons(TRUE);
}


void CCTCViewerDlg::OnBnClickedPrev()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);
	bool ret = GetPrevIndex();
	if (!ret)
	{
		AfxMessageBox(_T("Reached the beginning of Region List"));
	}
	else
	{
		CString indexStr;
		indexStr.Format(_T("%d"), m_DisplayedRgnIndex);
		m_CTCIndex.SetWindowTextW(indexStr);
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		ResetMag1();
		LoadRegionImageFile(m_DisplayedRgnIndex - 1, m_Mag1.GetPos());
	}
	EnableButtons(TRUE);
}

void CCTCViewerDlg::CopyToRGBPad(unsigned int color)
{
	if (m_Pad != NULL)
	{
		m_Pad.Destroy();
	}
	int nHeight = 48;
	int nWidth = 48;
	m_Pad.Create(nWidth, -nHeight, 24);

	BYTE *pCursor = (BYTE*)m_Pad.GetBits();
	int nStride = m_Pad.GetPitch() - (nWidth * 3);

	BYTE blue = ((color >> 16) & 0xFF);
	BYTE green = ((color >> 8) & 0xFF);
	BYTE red = (color & 0xFF);
	for (int y = 0; y<nHeight; y++)
	{
		for (int x = 0; x<nWidth; x++)
		{
			*pCursor++ = blue;
			*pCursor++ = green;
			*pCursor++ = red;
		}
		if (nStride > 0)
			pCursor += nStride;
	}
}

void CCTCViewerDlg::FreeRGNData(vector<CRGNData *> *rgnList)
{
	if (rgnList->size() != 0)
	{
		for (int i = 0; i < (int) rgnList->size(); i++)
		{
			delete (*rgnList)[i];
			(*rgnList)[i] = NULL;
		}
		rgnList->clear();
	}
}

BOOL  CCTCViewerDlg::DisplayROI(int index)
{
	BOOL ret = FALSE;
	CString message;

	DisplayComment(index);
	int index1 = index - 1;
	unsigned int color = m_RGNDataArray[index1]->GetColorCode();
	if (color == CTC)
	{
		m_WBCsRadio.SetCheck(BST_UNCHECKED);
		m_ConfirmedCTCRadio.SetCheck(BST_UNCHECKED);
		m_NonCKRadio.SetCheck(BST_UNCHECKED);
	}
	else if (color == CTC2)
	{
		m_WBCsRadio.SetCheck(BST_UNCHECKED);
		m_NonCKRadio.SetCheck(BST_UNCHECKED);
		m_ConfirmedCTCRadio.SetCheck(BST_CHECKED);
	}
	else if (color == NONCTC)
	{
		if (m_RGNDataArray[index1]->m_ReviewAnswer[NONCK])
		{
			m_ConfirmedCTCRadio.SetCheck(BST_UNCHECKED);
			m_WBCsRadio.SetCheck(BST_UNCHECKED);
			m_NonCKRadio.SetCheck(BST_CHECKED);
		}
		else if (m_RGNDataArray[index1]->m_ReviewAnswer[WBCSHAPE])
		{
			m_NonCKRadio.SetCheck(BST_UNCHECKED);
			m_ConfirmedCTCRadio.SetCheck(BST_UNCHECKED);
			m_WBCsRadio.SetCheck(BST_CHECKED);
		}
	}
	CopyToRGBPad(color);
	RECT rect1;
	m_ColorPadDisplay.GetWindowRect(&rect1);
	ScreenToClient(&rect1);
	InvalidateRect(&rect1, false);

	unsigned short *blue = m_BlueRegionImage;
	unsigned short *green = m_GreenRegionImage;
	unsigned short *red = m_RedRegionImage;
	if ((blue == NULL) || (green == NULL) || (red == NULL))
	{
		message.Format(_T("Region No. %d Image Data has not been loaded"), index);
		m_Log.Message(message);
		m_Status = message;
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
	else
	{
		ret = TRUE;
		if (m_Image != NULL)
		{
			m_Image.Destroy();
			m_ImageWidth = 0;
			m_ImageHeight = 0;
		}
		m_ImageWidth = m_RegionWidth;
		m_ImageHeight = m_RegionHeight;
		m_Image.Create(m_ImageWidth, -m_ImageHeight, 24);

		CopyToRGBImage((m_Blue.GetCheck() ? blue : NULL), (m_Green.GetCheck() ? green : NULL), (m_Red.GetCheck() ? red : NULL), 
			&m_Image, m_RGNDataArray[index1]);

		RECT rect1;
		m_ImageDisplay.GetWindowRect(&rect1);
		ScreenToClient(&rect1);
		InvalidateRect(&rect1, false);
		m_Status.Format(_T("Displayed ROI Image of Rgn No.%d, UpLeftCorner: X=%d,Y=%d"), index, m_RegionX0, m_RegionY0);
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
	return ret;
}

void CCTCViewerDlg::OnBnClickedSavergn()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	UpdateData(TRUE);
	if (m_RGNDataArray.size() > 0)
	{
		if (m_ReviewerName.GetLength() == 0)
		{
			MessageBox(_T("Please enter Reviewer Name before saving region file"));
			return;
		}
		else if ((m_ReviewerNameInRGNFile.Compare(_T("temp")) != 0) && (m_ReviewerName.Compare(m_ReviewerNameInRGNFile) != 0))
		{
			CString message;
			message.Format(_T("Original Reviewer=%s (Current Reviewer=%s), Overwrite Reviewer Name with Current Reviewer Name?"),
				m_ReviewerNameInRGNFile, m_ReviewerName);
			int answer = AfxMessageBox(message, MB_YESNO);
			if (answer == IDNO)
				return;
		}
		SaveRGNFile();
	}
	else
	{
		m_Status = "Cell Region # is 0";
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
}

void CCTCViewerDlg::OnBnClickedConfirmedctc()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);
	m_ConfirmedCTCRadio.SetCheck(BST_CHECKED);
	if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
	{
		m_RGNDataArray[m_DisplayedRgnIndex - 1]->m_ReviewAnswer[NONCK] = false;
		m_RGNDataArray[m_DisplayedRgnIndex - 1]->m_ReviewAnswer[CONFIRMED] = true;
		m_RGNDataArray[m_DisplayedRgnIndex - 1]->m_ReviewAnswer[WBCSHAPE] = false;
		m_RGNDataArray[m_DisplayedRgnIndex - 1]->SetColorCode(CTC2);
		UpdateColorCodeSelection();
	}
	EnableButtons(TRUE);
}

void CCTCViewerDlg::UpdateColorCodeSelection(void)
{
	if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
	{
		CopyToRGBPad(m_RGNDataArray[m_DisplayedRgnIndex - 1]->GetColorCode());
		RECT rect1;
		m_ColorPadDisplay.GetWindowRect(&rect1);
		ScreenToClient(&rect1);
		InvalidateRect(&rect1, false);
		CountColorCode();
	}
}

BOOL CCTCViewerDlg::SaveRGNFile()
{
	BOOL ret = FALSE;
	CString filenameForSave;
	CString defaultPathname;
	CString defaultFilename;

	WCHAR *char1 = _T("\\");
	int index = m_FullPathTIFFFilename.ReverseFind(*char1);
	defaultPathname = m_FullPathTIFFFilename.Mid(0, index + 1);
	defaultFilename.Format(_T("%s%s_Reviewer%s.rgn"), defaultPathname, m_SampleName, m_ReviewerName);

	CFileDialog dlg(FALSE,    // save
		_T(".rgn"), 
		defaultFilename,
		OFN_OVERWRITEPROMPT,
		_T("RGN files (*.rgn)|*.rgn||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		filenameForSave = dlg.GetPathName();
		WCHAR *char1 = _T(".");
		CString filename1 = filenameForSave;
		int filename1Index = filename1.ReverseFind(*char1);
		if (!m_UsingHitImageData)
		{
			CString hitFilename = filename1.Mid(0, filename1Index + 1) + _T("hit");
			SaveHitFile(hitFilename);
		}
		CString commentFilename = filename1.Mid(0, filename1Index + 1) + _T("txt");
		SaveComments(commentFilename);
		CString checklistFilename = filename1.Mid(0, filename1Index + 1) + _T("at5");
		SaveCheckListData(checklistFilename);

		CStdioFile theFile;

		if (theFile.Open(filenameForSave, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
		{
			for (int i = 0; i < (int) m_RGNDataArray.size(); i++)
			{
				CString singleLine;
				int x0, y0;
				m_RGNDataArray[i]->GetPosition(&x0, &y0);
				singleLine.Format(_T("0 1, 1 %lu, 2 %d %d, 3 0 0, 4 0, 5 1, 6 2 100 100, 7 %d\n"),
					m_RGNDataArray[i]->GetColorCode(), x0, y0, i + 1);
				theFile.WriteString(singleLine);
			}
			theFile.Close();
			m_Status.Format(_T("%s Saved."), dlg.GetFileName());
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		}
		else
		{
			m_Status.Format(_T("Failed to Open %s to save."), dlg.GetFileName());
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		}
	}
	return ret;
}

void CCTCViewerDlg::OnBnClickedSelect()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);
	CString indexStr; 
	m_CTCIndex.GetWindowTextW(indexStr);
	int newIndex = _wtoi(indexStr);
	if ((newIndex > 0) && (newIndex <= m_CTCCount))
	{
		m_DisplayedRgnIndex = newIndex;
		UpdateColorCodeSelection();
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		ResetMag1();
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		LoadRegionImageFile(m_DisplayedRgnIndex - 1, m_Mag1.GetPos());
	}
	else
	{
		CString message;
		message.Format(_T("Region No.%d is out of the range for Region Index (1..%d)"), newIndex, m_CTCCount);
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		indexStr.Format(_T("%d"), m_DisplayedRgnIndex);
		m_CTCIndex.SetWindowTextW(indexStr);
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
	EnableButtons(TRUE);
}

void CCTCViewerDlg::ResetSlider()
{
	m_BlueSlider.SetPos(120);
	m_GreenSlider.SetPos(120);
	m_RedSlider.SetPos(120);
}

void CCTCViewerDlg::ResetMag1()
{
	m_Mag1.SetPos(MAX_MAGNIFICATION);
	m_CellScoreList.DeleteAllItems();
}

void CCTCViewerDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO:  在此加入您的訊息處理常式程式碼和 (或) 呼叫預設值
	EnableButtons(FALSE);
	if ((pScrollBar == (CScrollBar *)&m_BlueSlider) ||
		(pScrollBar == (CScrollBar *)&m_GreenSlider) ||
		(pScrollBar == (CScrollBar *)&m_RedSlider) ||
		(pScrollBar == (CScrollBar *)&m_Mag1))
	{
		if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
		{
			if (pScrollBar == (CScrollBar *)&m_Mag1)
			{
				LoadRegionImageFile(m_DisplayedRgnIndex - 1, m_Mag1.GetPos());
			}
			else
			{
				if (pScrollBar == (CScrollBar *)&m_BlueSlider)
					m_Blue.SetCheck(TRUE);
				else if (pScrollBar == (CScrollBar *)&m_GreenSlider)
					m_Green.SetCheck(TRUE);
				else if (pScrollBar == (CScrollBar *)&m_RedSlider)
					m_Red.SetCheck(TRUE);
				DisplayROI(m_DisplayedRgnIndex);
			}
		}
	}
	else {
		CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
	}
	EnableButtons(TRUE);
}

BYTE CCTCViewerDlg::GetContrastEnhancedByte(unsigned short value, int maxValue, PIXEL_COLOR_TYPE color, int backGroundIntensity)
{
	BYTE result = 0;
	int intensity = 0;
	if (color == BLUE_COLOR)
	{
		intensity = backGroundIntensity;
		intensity += m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF];
	}
	else if (color == GREEN_COLOR)
	{
		intensity = backGroundIntensity;
		intensity += m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF];
	}
	else if (color == RED_COLOR)
	{
		intensity = backGroundIntensity;
		intensity += m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF];
	}
	int maxInten = 100;
	if (color == BLUE_COLOR)
	{
		maxInten = m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST] - intensity;
	}
	else if (color == GREEN_COLOR)
	{
		maxInten = m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST] - intensity;
	}
	else if (color == RED_COLOR)
	{
		maxInten = m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST] - intensity;
	}
	
	if (maxValue > 120)
	{
		maxInten = (int) (((4095.0 - maxInten) * (maxValue - 120.0) / 130.0) + maxInten);
	}
	else if (maxValue < 120)
	{
		maxInten = (int) (maxInten * maxValue / 120.0);
	}
	
	int value1 = value;
	value1 -= intensity;
	if (value1 < 0)
		value1 = 0;
	if (value1 >= maxInten)
		result = (BYTE)255;
	else
		result = (BYTE)(255 * value1 / maxInten);
	return result;
}

CString CCTCViewerDlg::GetCommentContents(int rgnIndex)
{
	bool found = false;
	CString textline = _T("");
	for (int i = 0; i < (int)m_Comments.size(); i++)
	{
		textline = m_Comments[i];
		int index = textline.Find(',');
		if (index != -1)
		{
			CString numStr = textline.Mid(0, index);
			int rgnNum = _wtoi(numStr);
			if (rgnNum == rgnIndex)
			{
				textline = textline.Mid(index + 1);
				found = true;
				break;
			}
		}
	}
	if (found)
		return textline;
	else
		return _T("");
}

BOOL CCTCViewerDlg::PreTranslateMessage(MSG* pMsg)
{
	CEdit *redCutoff = (CEdit *)GetDlgItem(IDC_REDCUTOFF);
	CEdit *greenCutoff = (CEdit *)GetDlgItem(IDC_GREENCUTOFF);
	CEdit *blueCutoff = (CEdit *)GetDlgItem(IDC_BLUECUTOFF);
	CEdit *redContrast = (CEdit *)GetDlgItem(IDC_REDCONTRAST);
	CEdit *greenContrast = (CEdit *)GetDlgItem(IDC_GREENCONTRAST);
	CEdit *blueContrast = (CEdit *)GetDlgItem(IDC_BLUECONTRAST);
	CEdit *ctcIndex = (CEdit *)GetDlgItem(IDC_REGIONNUM);
	CEdit *bRedThre = (CEdit *)GetDlgItem(IDC_BREDTHR);
	CEdit *bBlueThre = (CEdit *)GetDlgItem(IDC_BBLUETHR);
	CEdit *bGreenThre = (CEdit *)GetDlgItem(IDC_BGREENTHR);
	CEdit *bMaxCount = (CEdit *)GetDlgItem(IDC_MAXBLOBPIXELCOUNT);
	CEdit *bMinCount = (CEdit *)GetDlgItem(IDC_MINBLOBPIXELCOUNT);
	CEdit *bComments = (CEdit *)GetDlgItem(IDC_COMMENT);
	CEdit *bReviewerName = (CEdit *)GetDlgItem(IDC_REVIEWER);

	if ((pMsg->message == WM_KEYDOWN) &&
		(pMsg->wParam == VK_RETURN) &&
		((GetFocus() == redCutoff) ||
		(GetFocus() == greenCutoff) ||
		(GetFocus() == blueCutoff) ||
		(GetFocus() == redContrast) ||
		(GetFocus() == greenContrast) ||
		(GetFocus() == blueContrast) ||
		(GetFocus() == ctcIndex) ||
		(GetFocus() == bRedThre) ||
		(GetFocus() == bBlueThre) ||
		(GetFocus() == bGreenThre) ||
		(GetFocus() == bMaxCount) ||
		(GetFocus() == bMinCount) ||
		(GetFocus() == bComments) ||
		(GetFocus() == bReviewerName)))
	{
		EnableButtons(FALSE);
		UpdateData(TRUE);
		if (GetFocus() == bComments)
		{
			CString comment = m_Comment;
			bool updated = false;
			CString textline;
			for (int i = 0; i < (int)m_Comments.size(); i++)
			{
				textline = m_Comments[i];
				int index = textline.Find(',');
				if (index != -1)
				{
					CString numStr = textline.Mid(0, index);
					int rgnNum = _wtoi(numStr);
					if (rgnNum == m_DisplayedRgnIndex)
					{
						textline.Format(_T("%d,%s"), m_DisplayedRgnIndex, comment);
						m_Comments[i] = textline;
						updated = true;
						break;
					}
				}
			}
			if (!updated)
			{
				textline.Format(_T("%d,%s"), m_DisplayedRgnIndex, comment);
				m_Comments.push_back(textline);
			}
			SaveComments(m_CommentFileName);
		}
		if ((m_DisplayedRgnIndex > 0) && ((m_DisplayedRgnIndex - 1) < (int) m_RGNDataArray.size()))
			DisplayROI(m_DisplayedRgnIndex);
		EnableButtons(TRUE);
		return TRUE; // this doesn't need processing anymore
	}
	else if ((pMsg->message == WM_KEYDOWN) &&
			(pMsg->wParam == VK_RETURN))
	{
		return TRUE;
	}
	else if ((m_UseKeyStroke.GetCheck() == BST_CHECKED) 
		&& (pMsg->message == WM_KEYDOWN) &&
		(GetFocus() != bComments) &&
		((pMsg->wParam == 'Q') || (pMsg->wParam == 'q') ||
		(pMsg->wParam == 'W') || (pMsg->wParam == 'w') || 
		(pMsg->wParam == 'E') || (pMsg->wParam == 'e')))
	{
		EnableButtons(FALSE);
		if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
		{
			m_RGNDataArray[m_DisplayedRgnIndex - 1]->m_ReviewAnswer[NONCK] = false;
			m_RGNDataArray[m_DisplayedRgnIndex - 1]->m_ReviewAnswer[CONFIRMED] = false;
			m_RGNDataArray[m_DisplayedRgnIndex - 1]->m_ReviewAnswer[WBCSHAPE] = false;
			switch (pMsg->wParam)
			{
			case 'Q':
			case 'q':
				m_RGNDataArray[m_DisplayedRgnIndex - 1]->m_ReviewAnswer[NONCK] = true;
				m_RGNDataArray[m_DisplayedRgnIndex - 1]->SetColorCode(NONCTC);
				break;
			case 'W':
			case 'w':
				m_RGNDataArray[m_DisplayedRgnIndex - 1]->m_ReviewAnswer[CONFIRMED] = true;
				m_RGNDataArray[m_DisplayedRgnIndex - 1]->SetColorCode(CTC2);
				break;
			case 'E':
			case 'e':
				m_RGNDataArray[m_DisplayedRgnIndex - 1]->m_ReviewAnswer[WBCSHAPE] = true;
				m_RGNDataArray[m_DisplayedRgnIndex - 1]->SetColorCode(NONCTC);
				break;
			}
		}
		CountColorCode();
		bool result = GetNextIndex();
		if (result)
		{
			CString indexStr;
			indexStr.Format(_T("%d"), m_DisplayedRgnIndex);
			m_CTCIndex.SetWindowTextW(indexStr);
			ResetMag1();
			LoadRegionImageFile(m_DisplayedRgnIndex - 1, m_Mag1.GetPos());
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		}
		else
		{
			AfxMessageBox(_T("Reached the end of Region List"));
		}
		EnableButtons(TRUE);
		return TRUE;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CCTCViewerDlg::DrawROIWindow(CDC* dc, RECT rc)
{
	CPen CursorPen(PS_SOLID, 3, RGB(255, 255, 255)); // red (255,0,0), green (0.255,0), blue (0.0.255)

	CPen *pOldPen = dc->SelectObject(&CursorPen);
	CBrush brNull;
	LOGBRUSH lb;
	lb.lbStyle = BS_NULL;
	brNull.CreateBrushIndirect(&lb);
	dc->SelectObject(&brNull);
	dc->Ellipse(rc.left, rc.top, rc.right, rc.bottom);
	dc->SelectObject(pOldPen);
}

void CCTCViewerDlg::ResetBoxPos()
{
	RECT rect;
	m_ImageDisplay.GetClientRect(&rect);
	m_ROIRect.left = 20;
	m_ROIRect.bottom = rect.bottom - 20;
	m_BoxWidth = (int) ((rect.right - rect.left) * (10.0 / 0.648) / m_RegionWidth);
	m_BoxHeight = (int) ((rect.bottom - rect.top) * (10.0 / 0.648) / m_RegionHeight);
	m_ROIRect.right = m_ROIRect.left + m_BoxWidth;
	m_ROIRect.top = m_ROIRect.bottom - m_BoxHeight;	
}

void CCTCViewerDlg::DisplayBoxDescription(void)
{
	int width, height;

	width = (int) (10.0 / 0.648);
	height = (int) (10.0 / 0.648);
	int x0, y0;
	int red, green, blue;
	int redmax, greenmax, bluemax;
	GetAverageIntensity(&x0, &y0, width, height, &red, &green, &blue, &redmax, &greenmax, &bluemax);
	m_Description.Format(_T("CPI:RGB(%d,%d,%d),Radius=5(microns),Center(%d,%d),Intensity(mean,max)=R(%d,%d),G(%d,%d)[Circle]"), 
		m_RedIntensity, m_GreenIntensity, m_BlueIntensity, x0, y0, red, redmax, 
		green, greenmax, blue, bluemax);
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
}

void CCTCViewerDlg::GetAverageIntensity(int *x0, int *y0, int width, int height, int *red, int *green, int *blue, int *redmax, int *greenmax, int *bluemax)
{
	*x0 = 0;
	*y0 = 0;
	*red = 0;
	*green = 0;
	*blue = 0;
	*redmax = 0;
	*greenmax = 0;
	*bluemax = 0;
	RECT rect;
	m_ImageDisplay.GetClientRect(&rect);
	double xRatio = ((double) m_RegionWidth) / (rect.right - rect.left);
	double yRatio = ((double)m_RegionHeight) / (rect.bottom - rect.top);
	*x0 = (int) (m_ROIRect.left * xRatio);
	int width1 = m_RegionWidth;
	*y0 = (int)(m_ROIRect.top * yRatio);
	UpdateData(TRUE);
	CalculateAverageIntensity(m_RedRegionImage, width1, *x0, *y0, width, height, RED_COLOR, red, redmax);
	CalculateAverageIntensity(m_GreenRegionImage, width1, *x0, *y0, width, height, GREEN_COLOR, green, greenmax);
	CalculateAverageIntensity(m_BlueRegionImage, width1, *x0, *y0, width, height, BLUE_COLOR, blue, bluemax);
}

void  CCTCViewerDlg::CalculateAverageIntensity(unsigned short *image, int width1, int x0, int y0, int width, int height,
	PIXEL_COLOR_TYPE color, int *average, int *max)
{
	int sum = 0;
	int max1 = 0;
	int count = 0;

	int xCenter = width / 2;
	int yCenter = height / 2;
	int radius2 = xCenter * xCenter;
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			int dx = j - xCenter;
			dx *= dx;
			int dy = i - yCenter;
			dy *= dy;
			if ((dx + dy) < radius2)
			{
				unsigned short value = *(image + width1 * (i + y0) + (j + x0));
				sum += ((int)value);
				count++;
				if (((int)value) > max1)
					max1 = ((int)value);
			}
		}
	}

	*max = max1;
	*average = (int) (sum / count);
}

void CCTCViewerDlg::OnBnClickedSaveimage()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	BOOL ret = FALSE;
	CString filename;
	CString message;

	CFileDialog dlg(FALSE,    // save
		CString(".bmp"),    // no default extension
		NULL,    // no initial file name
		OFN_OVERWRITEPROMPT,
		_T("Image files (*.bmp)|*.bmp||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		filename = dlg.GetPathName();
		CString filename3 = dlg.GetFileName();
		BOOL ret = Save4BMPFiles(filename);
		if (ret)
		{
			message.Format(_T("4 BMP Files are saved."));
		}
		else
		{
			message.Format(_T("Failed to save 4 BMP Files."));
		}
	}
	else
	{
		message.Format(_T("Failed to specify a filename for saving image"));
	}
	m_Status = message;
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	m_Log.Message(message);
	return;
}

BOOL CCTCViewerDlg::SaveBMPImage(CString filename, CImage *image)
{
	BOOL ret = FALSE;

	CFile fileToSave;
	ret = fileToSave.Open(filename, CFile::modeCreate | CFile::modeWrite);
	if (ret)
	{
		BITMAPINFOHEADER BMIH;
		memset(&BMIH, 0, 40);
		BMIH.biSize = 40;
		BMIH.biSizeImage = image->GetPitch() * image->GetHeight();
		BMIH.biWidth = image->GetWidth();
		BMIH.biHeight = image->GetHeight();
		if (BMIH.biHeight > 0)
			BMIH.biHeight = -BMIH.biHeight;
		BMIH.biPlanes = 1;
		BMIH.biBitCount = 24;
		BMIH.biCompression = BI_RGB;

		BITMAPFILEHEADER bmfh;
		memset(&bmfh, 0, 14);
		int nBitsOffset = 14 + BMIH.biSize;
		LONG lImageSize = BMIH.biSizeImage;
		LONG lFileSize = nBitsOffset + lImageSize;

		bmfh.bfType = 'B' + ('M' << 8);
		bmfh.bfOffBits = nBitsOffset;
		bmfh.bfSize = lFileSize;
		bmfh.bfReserved1 = bmfh.bfReserved2 = 0;

		fileToSave.Write(&bmfh, 14);
		fileToSave.Write(&BMIH, 40);
		fileToSave.Write(image->GetBits(), lImageSize);
		fileToSave.Close();
	}

	return ret;
}

void CCTCViewerDlg::LoadRegionImageFile(int index, int mag)
{
	SaveTempRegionFile();
	CString message;

	int X0, Y0;
	if ((index >= 0) && (index < (int)m_RGNDataArray.size()))
	{
		FreeRegionImageData();
		BOOL redRet = FALSE;
		BOOL greenRet = FALSE;
		BOOL blueRet = FALSE;
		if (m_UsingHitImageData == false)
		{
			m_RGNDataArray[index]->GetPosition(&X0, &Y0);
			m_RegionWidth = 1000 / mag;
			m_RegionHeight = 1000 / mag;
			m_RegionX0 = X0 + 50 - m_RegionWidth / 2;
			m_RegionY0 = Y0 + 50 - m_RegionHeight / 2;
			ResetBoxPos();
			m_RedIntensity = m_RedTIFFData->GetCPI();
			m_RedRegionImage = new unsigned short[m_RegionWidth * m_RegionHeight];
			redRet = m_RedTIFFData->GetImageRegion(m_RegionX0, m_RegionY0, m_RegionWidth, m_RegionHeight, m_RedRegionImage);
			m_GreenIntensity = m_GreenTIFFData->GetCPI();
			m_GreenRegionImage = new unsigned short[m_RegionWidth * m_RegionHeight];
			greenRet = m_GreenTIFFData->GetImageRegion(m_RegionX0, m_RegionY0, m_RegionWidth, m_RegionHeight, m_GreenRegionImage);
			m_BlueIntensity = m_BlueTIFFData->GetCPI();
			m_BlueRegionImage = new unsigned short[m_RegionWidth * m_RegionHeight];
			blueRet = m_BlueTIFFData->GetImageRegion(m_RegionX0, m_RegionY0, m_RegionWidth, m_RegionHeight, m_BlueRegionImage);
		}
		else
		{
			m_RGNDataArray[index]->GetPosition(&X0, &Y0);
			CRGNData *rgnPtr = m_RGNDataArray[index];
			if (mag == 10)
			{
				m_RegionWidth = 100;
				m_RegionHeight = 100;
				m_RegionX0 = X0;
				m_RegionY0 = Y0;
				ResetBoxPos();
			}
			else
			{
				int newMag = 2 * (mag - 1) + 10;
				m_RegionWidth = 3000 / newMag;
				m_RegionHeight = 3000 / newMag;
				m_RegionX0 = X0 + 50 - m_RegionWidth / 2;
				m_RegionY0 = Y0 + 50 - m_RegionHeight / 2;
				ResetBoxPos();
			}				
			m_RedIntensity = rgnPtr->GetCPI(RED_COLOR);
			m_GreenIntensity = rgnPtr->GetCPI(GREEN_COLOR);
			m_BlueIntensity = rgnPtr->GetCPI(BLUE_COLOR);
			m_RedRegionImage = new unsigned short[m_RegionWidth * m_RegionHeight];
			m_GreenRegionImage = new unsigned short[m_RegionWidth * m_RegionHeight];
			m_BlueRegionImage = new unsigned short[m_RegionWidth * m_RegionHeight];
			int x1 = m_RegionX0 - rgnPtr->m_ZoomOutX0;
			int y1 = m_RegionY0 - rgnPtr->m_ZoomOutY0;
			for (int i = 0; i < m_RegionHeight; i++)
			{
				memcpy(m_RedRegionImage + m_RegionWidth * i, rgnPtr->m_ZoomOutRed + rgnPtr->m_ZoomOutWidth * (y1 + i) + x1, sizeof(unsigned short) * m_RegionWidth);
				memcpy(m_GreenRegionImage + m_RegionWidth * i, rgnPtr->m_ZoomOutGreen + rgnPtr->m_ZoomOutWidth * (y1 + i) + x1, sizeof(unsigned short) * m_RegionWidth);
				memcpy(m_BlueRegionImage + m_RegionWidth * i, rgnPtr->m_ZoomOutBlue + rgnPtr->m_ZoomOutWidth * (y1 + i) + x1, sizeof(unsigned short) * m_RegionWidth);
			}
			redRet = TRUE;
			greenRet = TRUE;
			blueRet = TRUE;
		}
		if (redRet && greenRet && blueRet)
		{
			if (mag == 10)
				AutoScreen(index);
			DisplayROI(index + 1);
		}
		else
		{
			message.Format(_T("Failed to get Image Data for Region No.%d"), index + 1);
			m_Status = message;
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			m_Log.Message(message);
		}
	}
}

void CCTCViewerDlg::OnBnClickedRedreset()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);
	m_RedSlider.SetPos(120);
	if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
		DisplayROI(m_DisplayedRgnIndex);
	EnableButtons(TRUE);
}


void CCTCViewerDlg::OnBnClickedGreenreset()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);
	m_GreenSlider.SetPos(120);
	if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
		DisplayROI(m_DisplayedRgnIndex);
	EnableButtons(TRUE);
}


void CCTCViewerDlg::OnBnClickedBluereset()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);
	m_BlueSlider.SetPos(120);
	if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
		DisplayROI(m_DisplayedRgnIndex);
	EnableButtons(TRUE);
}


void CCTCViewerDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO:  在此加入您的訊息處理常式程式碼和 (或) 呼叫預設值
	UpdateData(TRUE);
	EnableButtons(FALSE);
	RECT rect;
	m_ImageDisplay.GetClientRect(&rect);
	int x0 = 27;
	int y0 = 13;
		
	if ((m_DisplayedRgnIndex > 0) &&
				((point.x - x0) >= rect.left) &&
				((point.y - y0) >= rect.top) &&
				((point.x - x0) < rect.right) &&
				((point.y - y0) < rect.bottom))
	{
		if (m_UseIntensityBox.GetCheck() == BST_UNCHECKED)
		{
			double xRatio = ((double)m_RegionWidth) / (rect.right - rect.left);
			double yRatio = ((double)m_RegionHeight) / (rect.bottom - rect.top);
			int x = (int)((point.x - x0) * xRatio);
			int width1 = m_RegionWidth;
			int y = (int)((point.y - y0) * yRatio);
			unsigned short red = *(m_RedRegionImage + width1 * y + x);
			unsigned short green = *(m_GreenRegionImage + width1 * y + x);
			unsigned short blue = *(m_BlueRegionImage + width1 * y + x);
			m_Description.Format(_T("CPI:RGB(%d,%d,%d), Pos(%d,%d), Intensity:RGB(%u,%d,%d)"), m_RedIntensity, m_GreenIntensity, m_BlueIntensity, x, y, red, green, blue);
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		}
		else if ((m_UseIntensityBox.GetCheck() == BST_CHECKED) &&
			((point.x - x0) < (rect.right - m_BoxWidth)) &&
			((point.y - y0) < (rect.bottom - m_BoxHeight)))
		{
			mouseIndex = 1;
			toggleFlag = !toggleFlag;
			m_CellScoreList.DeleteAllItems();
			m_ROIRect.left = point.x - x0 - m_BoxWidth / 2;;
			m_ROIRect.right = m_ROIRect.left + m_BoxWidth;
			m_ROIRect.top = point.y - y0 - m_BoxHeight / 2;
			m_ROIRect.bottom = m_ROIRect.top + m_BoxHeight;

				if (!toggleFlag)
				{
					unsigned short *blue = m_BlueRegionImage;
					unsigned short *green = m_GreenRegionImage;
					unsigned short *red = m_RedRegionImage;
					if (m_Image != NULL)
					{
						m_Image.Destroy();
						m_ImageWidth = 0;
						m_ImageHeight = 0;
					}
					m_ImageWidth = m_RegionWidth;
					m_ImageHeight = m_RegionHeight;
					m_Image.Create(m_ImageWidth, -m_ImageHeight, 24);

					CopyToRGBImage((m_Blue.GetCheck() ? blue : NULL), (m_Green.GetCheck() ? green : NULL), (m_Red.GetCheck() ? red : NULL), 
						&m_Image, m_RGNDataArray[m_DisplayedRgnIndex-1]);
					RECT rect1;
					m_ImageDisplay.GetWindowRect(&rect1);
					ScreenToClient(&rect1);
					InvalidateRect(&rect1, false);
					EnableButtons(TRUE);
					return;
				}

			int width, height;
			width = (int)(10.0 / 0.648);
			height = (int)(10.0 / 0.648);
			int x1, y1;
			int redAve, greenAve, blueAve;
			int redmax, greenmax, bluemax;
			GetAverageIntensity(&x1, &y1, width, height, &redAve, &greenAve, &blueAve, &redmax, &greenmax, &bluemax);
			int redValue = redAve - m_RedIntensity;
			int itemIndex = 0;
			int idx = 0;
			CString labelStr;
			labelStr = _T("RedRelative");
			idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
			labelStr.Format(_T("%d"), redValue);
			m_CellScoreList.SetItemText(idx, 1, labelStr);
			int greenValue = greenAve - m_GreenIntensity;
			labelStr = _T("GreenRelative");
			idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
			labelStr.Format(_T("%d"), greenValue);
			m_CellScoreList.SetItemText(idx, 1, labelStr);
			labelStr = _T("R/G");
			idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
			if ((redValue > 0) && (greenValue > 0))
			{
				labelStr.Format(_T("%.2lf"), ((double)redValue / ((double)greenValue)));
				m_CellScoreList.SetItemText(idx, 1, labelStr);
			}
			else if (redValue <= 0)
			{
				labelStr.Format(_T("0"));
				m_CellScoreList.SetItemText(idx, 1, labelStr);
			}
			else
			{
				labelStr.Format(_T("100"));
				m_CellScoreList.SetItemText(idx, 1, labelStr);
			}
			if ((redValue > 0) && (greenValue > 0) && (redValue > greenValue))
			{
				labelStr = _T("R/(R-G)");
				idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
				double RGRatio = ((double)redValue / ((double)(redValue - greenValue)));
				labelStr.Format(_T("%.2lf"), RGRatio);
				m_CellScoreList.SetItemText(idx, 1, labelStr);
				labelStr.Format(_T("<Threshold2(=%0.2lf)"), (double)m_CTCParams.m_GroupParams[(int)PARAM_COEF2_THRESHOLD - (int)PARAM_FLOAT_FIRST]);
				idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
				if (RGRatio < (double)m_CTCParams.m_GroupParams[(int)PARAM_COEF2_THRESHOLD - (int)PARAM_FLOAT_FIRST])
				{
					labelStr.Format(_T("YES"));
					m_CellScoreList.SetItemText(idx, 1, labelStr);
				}
				else
				{
					labelStr.Format(_T("No"));
					m_CellScoreList.SetItemText(idx, 1, labelStr);
				}
			}
			labelStr = _T("GreenCutoff");
			idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
			labelStr.Format(_T("%d"), m_CTCParams.m_WBCGreenRelative + m_CTCParams.m_CTCParams[(int)PARAM_WBCGREENMARKUP]);
			m_CellScoreList.SetItemText(idx, 1, labelStr);
			labelStr.Format(_T(">GreenCutoff(=%d)"), m_CTCParams.m_WBCGreenRelative + m_CTCParams.m_CTCParams[(int)PARAM_WBCGREENMARKUP]);
			idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
			if (greenValue > (m_CTCParams.m_WBCGreenRelative + m_CTCParams.m_CTCParams[(int)PARAM_WBCGREENMARKUP]))
			{
				labelStr.Format(_T("YES"));
				m_CellScoreList.SetItemText(idx, 1, labelStr);
			}
			else
			{
				labelStr.Format(_T("No"));
				m_CellScoreList.SetItemText(idx, 1, labelStr);
			}
			unsigned short *blue = m_BlueRegionImage;
			unsigned short *green = m_GreenRegionImage;
			unsigned short *red = m_RedRegionImage;
			if (m_Image != NULL)
			{
				m_Image.Destroy();
				m_ImageWidth = 0;
				m_ImageHeight = 0;
			}
			m_ImageWidth = m_RegionWidth;
			m_ImageHeight = m_RegionHeight;
			m_Image.Create(m_ImageWidth, -m_ImageHeight, 24);
			CopyToRGBImage((m_Blue.GetCheck() ? blue : NULL), (m_Green.GetCheck() ? green : NULL), (m_Red.GetCheck() ? red : NULL),
				&m_Image, NULL);
			RECT rect1;
			m_ImageDisplay.GetWindowRect(&rect1);
			ScreenToClient(&rect1);
			InvalidateRect(&rect1, false);
		}
	}
	EnableButtons(TRUE);
	CDialogEx::OnLButtonDown(nFlags, point);
}

void CCTCViewerDlg::CountColorCode(void)
{
	m_ConfirmedCTCNum = 0;
	m_ConfirmedNonCTCNum = 0;
	m_CTCNum = 0;
	for (int i = 0; i < (int)m_RGNDataArray.size(); i++)
	{
		unsigned int color = m_RGNDataArray[i]->GetColorCode();
		switch (color)
		{
		case CTC:
			m_CTCNum++;
			break;
		case CTC2:
			m_ConfirmedCTCNum++;
			break;
		case NONCTC:
			m_ConfirmedNonCTCNum++;
			break;
		}
	}
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
}

void CCTCViewerDlg::SaveTempRegionFile(void)
{
	CString message;
	CString filenameForSave = _T("C:\\CTCReviewerLog\\temp.rgn");
	CStdioFile theFile;

	if (theFile.Open(filenameForSave, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		for (int i = 0; i < (int)m_RGNDataArray.size(); i++)
		{
			CString singleLine;
			int x0, y0;
			m_RGNDataArray[i]->GetPosition(&x0, &y0);
			singleLine.Format(_T("0 1, 1 %lu, 2 %d %d, 3 0 0, 4 0, 5 1, 6 2 100 100, 7 %d\n"),
				m_RGNDataArray[i]->GetColorCode(), x0, y0, i + 1);
			theFile.WriteString(singleLine);
		}
		theFile.Close();
		message.Format(_T("%s Saved. DisplayedRgnIndex=%d"), filenameForSave, m_DisplayedRgnIndex);
		m_Log.Message(message);
	}
	else
	{
		message.Format(_T("Failed to Open %s to save. DisplayedRgnIndex=%d"), filenameForSave, m_DisplayedRgnIndex),
			m_Log.Message(message);
	}
	filenameForSave = _T("C:\\CTCReviewerLog\\temp.at5");
	SaveCheckListData(filenameForSave);
}

void CCTCViewerDlg::OnBnClickedRedfile()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	bool ret = false;
	bool loaded = false;
	CButton *btn = (CButton *)GetDlgItem(IDC_REDFILE);
	btn->EnableWindow(FALSE);
	btn = (CButton *)GetDlgItem(IDC_OPENRGN);
	btn->EnableWindow(FALSE);
	FreeRGNData(&m_RGNDataArray);
	m_CellScoreList.DeleteAllItems();
	m_RedIntensity = 0;
	m_GreenIntensity = 0;
	m_BlueIntensity = 0;
	m_CTCCount = 0;
	m_DisplayedRgnIndex = 0;
	m_CTCIndex.SetWindowTextW(_T("0"));
	m_ConfirmedCTCNum = 0;
	m_ConfirmedNonCTCNum = 0;
	m_CTCNum = 0;
	m_RGNFilename = _T("");
	m_TIFFFilename = _T("");
	m_FullPathTIFFFilename = _T("");
	m_SampleName = _T("");
	m_GreenFilename = _T("");
	m_BlueFilename = _T("");
	m_CommentFileName = _T("");
	m_Comment = _T("");
	m_UsingHitImageData = false;
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	m_Status = _T("Start to load Red Channel TIFF File or Hit File. Please wait...");
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	CString message;

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("TIFF or Hit files (*.tif, *.hit)|*.tif; *.hit"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		int tiffIndex = filename.Find(_T(".tif"));
		if (tiffIndex > -1)
		{
			btn = (CButton *)GetDlgItem(IDC_OPENRGN);
			btn->EnableWindow(TRUE);
		}
		else if (filename.Find(_T(".hit")) > -1)
		{
			if (ReadHitFile(filename))
			{
				m_UsingHitImageData = true;
				if (m_ZeissData)
				{
					message.Format(_T("ZeissParams%s.txt"), CELLMAPPLUS_VERSION);
					m_CTCParams.LoadCTCParams(message, &m_TwoThresholds);
				}
				else
				{
					message.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
					m_CTCParams.LoadCTCParams(message, &m_TwoThresholds);
				}
				loaded = true;
				LoadCheckListData(filename);
			}
			else
			{
				btn = (CButton *)GetDlgItem(IDC_REDFILE);
				btn->EnableWindow(TRUE);
				m_Status.Format(_T("Failed to read Hit File %s"), filename);
				::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				return;
			}
		}
		m_TIFFFilename = dlg.GetFileName();
		if (tiffIndex > -1)
		{
			ret = m_RedTIFFData->LoadRawTIFFFile(filename);
			if (ret)
			{
				message.Format(_T("Red TIFF File %s loaded successfully, CPI=%d"), filename, m_RedTIFFData->GetCPI());
				m_Log.Message(message);
				m_FullPathTIFFFilename = filename;
				if (m_RedTIFFData->IsZeissData())
				{
					int postfixIndex = m_TIFFFilename.Find(_T(".tif"));
					m_SampleName = m_TIFFFilename.Mid(0, postfixIndex);
				}
				else
				{
					m_SampleName = m_TIFFFilename.Mid(Leica_Red_Prefix.GetLength());
					int index = m_SampleName.Find(_T(".tif"));
					m_SampleName = m_SampleName.Mid(0, index);
				}
				if (m_SampleName.GetLength() > 0)
				{
					WCHAR *char1 = _T("\\");
					int index = m_FullPathTIFFFilename.ReverseFind(*char1);
					CString pathname1 = m_FullPathTIFFFilename.Mid(0, index + 1);
					CString wb3Filename;
					wb3Filename.Format(_T("%s%s.wb4"), pathname1, m_SampleName);
					CStdioFile theFile;
					if (theFile.Open(wb3Filename, CFile::modeRead | CFile::typeText))
					{
						CString textline;
						theFile.ReadString(textline);
						CString nameTag = _T("WBCGreenRelative=");
						index = textline.Find(nameTag);
						if (index > -1)
						{
							CString numStr = textline.Mid(index + nameTag.GetLength());
							m_CTCParams.m_WBCGreenRelative = _wtoi(numStr);
						}
						theFile.Close();
					}
				}
				if (m_LoadOneChannel.GetCheck() == BST_CHECKED)
				{
					m_Status = _T("Please load Green Channel TIFF File");
					::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
					if (ReadGreenfile())
					{
						m_Status = _T("Please load Blue Channel TIFF File");
						::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
						if (ReadBluefile())
						{
							m_Status = _T("Please load Region File");
							::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
							if (ReadRgnfile())
							{
								loaded = true;
								btn = (CButton *)GetDlgItem(IDC_OPENRGN);
								btn->EnableWindow(TRUE);
							}
							else
							{
								btn = (CButton *)GetDlgItem(IDC_REDFILE);
								btn->EnableWindow(TRUE);
								m_Status.Format(_T("Failed to read Region File %s"), m_RGNFilename);
								::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
								return;
							}
						}
						else
						{
							btn = (CButton *)GetDlgItem(IDC_REDFILE);
							btn->EnableWindow(TRUE);
							m_Status.Format(_T("Failed to read Blue Channel File %s"), m_BlueFilename);
							::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
							return;
						}
					}
					else
					{
						btn = (CButton *)GetDlgItem(IDC_REDFILE);
						btn->EnableWindow(TRUE);
						m_Status.Format(_T("Failed to read Green Channel File %s"), m_GreenFilename);
						::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
						return;
					}
				}
				else
				{
					int index = m_TIFFFilename.Find(Leica_Red_Prefix, 0);
					if (index == -1)
					{
						int postfixIndex = filename.Find(Zeiss_Red_Postfix);
						if (postfixIndex == -1)
						{
							if (Zeiss_Red_Postfix.Find(_T("_c3_ORG.tif")) > -1)
							{
								postfixIndex = filename.Find(_T("_c2_ORG.tif"));
							}
							else if (Zeiss_Red_Postfix.Find(_T("_c2_ORG.tif")) > -1)
							{
								postfixIndex = filename.Find(_T("_c3_ORG.tif"));
							}
						}
						if (postfixIndex == -1)
						{
							btn = (CButton *)GetDlgItem(IDC_REDFILE);
							btn->EnableWindow(TRUE);
							m_Status.Format(_T("Red TIFF Filename %s doesn't have PREFIX %s or POSTFIX %s"), m_TIFFFilename, Leica_Red_Prefix, Zeiss_Red_Postfix);
							::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
							return;
						}
						else
						{
							CString samplePathName = filename.Mid(0, postfixIndex);
							postfixIndex = m_TIFFFilename.Find(_T("_ORG.tif"));
							CString sampleName = m_TIFFFilename.Mid(0, (postfixIndex - 3));
							CString filename2 = samplePathName + Zeiss_Green_Postfix;
							m_GreenFilename = sampleName + Zeiss_Green_Postfix;
							::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
							BOOL ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
							if (ret)
							{
								filename2 = samplePathName + Zeiss_Blue_Postfix;
								m_BlueFilename = sampleName + Zeiss_Blue_Postfix;
								::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
								BOOL ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
								if (ret)
								{
									m_ZeissData = true;
									message.Format(_T("ZeissParams%s.txt"), CELLMAPPLUS_VERSION);
									m_CTCParams.LoadCTCParams(message, &m_TwoThresholds);
									m_Status = _T("Please load RGN File.");
									::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
									if (ReadRgnfile())
									{
										loaded = true;
										btn = (CButton *)GetDlgItem(IDC_OPENRGN);
										btn->EnableWindow(TRUE);
									}
									else
									{
										btn = (CButton *)GetDlgItem(IDC_REDFILE);
										btn->EnableWindow(TRUE);
										m_Status.Format(_T("Failed to read Region File %s"), m_RGNFilename);
										::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
										return;
									}
								}
								else
								{
									btn = (CButton *)GetDlgItem(IDC_REDFILE);
									btn->EnableWindow(TRUE);
									m_Status = _T("Failed to load Blue TIFF File");
									::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
									return;
								}
							}
							else
							{
								btn = (CButton *)GetDlgItem(IDC_REDFILE);
								btn->EnableWindow(TRUE);
								m_Status = _T("Failed to load Green TIFF File");
								::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
								return;
							}
						}
					}
					else
					{
						CString postfix = m_TIFFFilename.Mid(Leica_Red_Prefix.GetLength());
						index = filename.Find(Leica_Red_Prefix, 0);
						CString filename1 = filename.Mid(0, index);
						CString filename2;
						filename2.Format(_T("%s%s%s"), filename1, Leica_Green_Prefix, postfix);
						m_GreenFilename.Format(_T("%s%s"), Leica_Green_Prefix, postfix);
						::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
						ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
						if (!ret)
						{
							btn = (CButton *)GetDlgItem(IDC_REDFILE);
							btn->EnableWindow(TRUE);
							m_Status.Format(_T("Failed to load %s%s"), Leica_Green_Prefix, postfix);
							::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
							return;
						}
						else
						{
							message.Format(_T("Green TIFF File %s loaded successfully, Green CPI=%d"), filename2, m_GreenTIFFData->GetCPI());
							m_GreenIntensity = m_GreenTIFFData->GetCPI();
							m_Log.Message(message);
							filename1 = filename.Mid(0, index);
							filename2.Format(_T("%s%s%s"), filename1, Leica_Blue_Prefix, postfix);
							m_BlueFilename.Format(_T("%s%s"), Leica_Blue_Prefix, postfix);
							::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
							ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
							if (!ret)
							{
								btn = (CButton *)GetDlgItem(IDC_REDFILE);
								btn->EnableWindow(TRUE);
								m_Status.Format(_T("Failed to load %s%s"), Leica_Blue_Prefix, postfix);
								::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
								return;
							}
							else
							{
								m_ZeissData = false;
								message.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
								m_CTCParams.LoadCTCParams(message, &m_TwoThresholds);
								message.Format(_T("Blue TIFF File %s loaded successfully, Blue CPI=%d"), filename2, m_BlueTIFFData->GetCPI());
								m_Log.Message(message);
								m_Status = _T("Please load RGN File.");
								::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
								if (ReadRgnfile())
								{
									loaded = true;
									btn = (CButton *)GetDlgItem(IDC_OPENRGN);
									btn->EnableWindow(TRUE);
								}
								else
								{
									btn = (CButton *)GetDlgItem(IDC_REDFILE);
									btn->EnableWindow(TRUE);
									m_Status.Format(_T("Failed to read Region File %s"), m_RGNFilename);
									::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
									return;
								}
							}
						}
					}
				}
			}
			else
			{
				btn = (CButton *)GetDlgItem(IDC_REDFILE);
				btn->EnableWindow(TRUE);
				m_Status = _T("Failed to load Red Channel TIFF File.");
				::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
				return;
			}
		}
		if (loaded)
		{
			CountColorCode();
			m_CTCCount = (int)m_RGNDataArray.size();
			if (m_CTCCount > 0)
			{
				StartFromHeadOfList();
			}
			else
			{
				m_Status.Format(_T("# of Hits=%d in RGN File %s"), m_CTCCount, m_RGNFilename);
				::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			}
		}
	}
	else
	{
		btn = (CButton *)GetDlgItem(IDC_REDFILE);
		btn->EnableWindow(TRUE);
		m_Status = _T("Failed to select a file in OpenFileDialog.");
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		return;
	}
	btn = (CButton *)GetDlgItem(IDC_REDFILE);
	btn->EnableWindow(TRUE);
}


bool CCTCViewerDlg::ReadGreenfile()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	bool ret = false;
	m_Status = _T("Start to load Green Channel TIFF File. Please wait...");
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("TIFF files (*.tif)|*.tif||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		m_GreenFilename = dlg.GetFileName();
		ret = m_GreenTIFFData->LoadRawTIFFFile(filename);
		if (ret)
		{
			int width1 = m_RedTIFFData->GetImageWidth();
			int height1 = m_RedTIFFData->GetImageHeight();
			int width2 = m_GreenTIFFData->GetImageWidth();
			int height2 = m_GreenTIFFData->GetImageHeight();
			if ((width1 != width2) || (height1 != height2))
			{
				m_Status.Format(_T("RedWidth(%d) != GreenWidth(%d), or RedHeight(%d) != GreenHeight(%d)"), width1, width2, height1, height2);
			}
			else
			{
				ret = true;
				CString message;
				message.Format(_T("Green TIFF File %s loaded successfully, CPI=%d"), filename, m_GreenTIFFData->GetCPI());
				m_GreenIntensity = m_GreenTIFFData->GetCPI();
				m_Log.Message(message);
				m_Status = _T("Please load Blue Channel TIFF File");
			}
		}
		else
			m_Status = _T("Failed to load Green Channel TIFF File.");
	}
	else
		m_Status = _T("Failed to select a file in OpenFileDialog.");
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	return ret;
}


bool CCTCViewerDlg::ReadBluefile()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	bool ret = false;
	m_Status = _T("Start to load Blue Channel TIFF File. Please wait...");
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("TIFF files (*.tif)|*.tif||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		m_BlueFilename = dlg.GetFileName();
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		ret = m_BlueTIFFData->LoadRawTIFFFile(filename);
		if (ret)
		{
			int width1 = m_RedTIFFData->GetImageWidth();
			int height1 = m_RedTIFFData->GetImageHeight();
			int width2 = m_BlueTIFFData->GetImageWidth();
			int height2 = m_BlueTIFFData->GetImageHeight();
			if ((width1 != width2) || (height1 != height2))
			{
				m_Status.Format(_T("RedWidth(%d) != BlueWidth(%d), or RedHeight(%d) != BlueHeight(%d)"), width1, width2, height1, height2);
			}
			else
			{
				CString message;
				if (m_BlueTIFFData->IsZeissData())
				{
					m_ZeissData = true;
					message.Format(_T("ZeissParams%s.txt"), CELLMAPPLUS_VERSION);
					m_CTCParams.LoadCTCParams(message, &m_TwoThresholds);
				}
				else
				{
					m_ZeissData = false;
					message.Format(_T("LeicaParams%s.txt"), CELLMAPPLUS_VERSION);
					m_CTCParams.LoadCTCParams(message, &m_TwoThresholds);
				}
				message.Format(_T("Blue TIFF File %s loaded successfully, CPI=%d"), filename, m_BlueTIFFData->GetCPI());
				m_Log.Message(message);
				m_Status = _T("Please load RGN File.");
				ret = true;
			}
		}
		else
			m_Status = _T("Failed to load Blue Channel TIFF File.");
	}
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	return ret;
}


void CCTCViewerDlg::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO:  在此加入您的訊息處理常式程式碼和 (或) 呼叫預設值
	UpdateData(TRUE);
	EnableButtons(FALSE);
	mouseIndex = 2;
	m_CellScoreList.DeleteAllItems();
	unsigned short *blue = m_BlueRegionImage;
	unsigned short *green = m_GreenRegionImage;
	unsigned short *red = m_RedRegionImage;
	if (m_Image != NULL)
	{
		m_Image.Destroy();
		m_ImageWidth = 0;
		m_ImageHeight = 0;
	}
	m_ImageWidth = m_RegionWidth;
	m_ImageHeight = m_RegionHeight;
	m_Image.Create(m_ImageWidth, -m_ImageHeight, 24);
	CRGNData *rgnPtr = m_RGNDataArray[m_DisplayedRgnIndex - 1];
	m_HitFinder.FreeBlobList(rgnPtr->GetBlobData(RED_COLOR));
	m_HitFinder.FreeBlobList(rgnPtr->GetBlobData(GREEN_COLOR));
	m_HitFinder.FreeBlobList(rgnPtr->GetBlobData(BLUE_COLOR));
	m_HitFinder.FreeBlobList(rgnPtr->GetBlobData(GB_COLORS));
	m_HitFinder.FreeBlobList(rgnPtr->GetBlobData(RB_COLORS));
	CopyToRGBImage((m_Blue.GetCheck() ? blue : NULL), (m_Green.GetCheck() ? green : NULL), (m_Red.GetCheck() ? red : NULL), 
		&m_Image, m_RGNDataArray[m_DisplayedRgnIndex-1]);
	RECT rect1;
	m_ImageDisplay.GetWindowRect(&rect1);
	ScreenToClient(&rect1);
	InvalidateRect(&rect1, false);
	toggleFlag = !toggleFlag;
	if (!toggleFlag)
	{
		EnableButtons(TRUE);
		return;
	}

	RECT rect;
	m_ImageDisplay.GetClientRect(&rect);
	int x0 = 27;
	int y0 = 13;
	if ((m_DisplayedRgnIndex > 0) &&
		(m_RegionWidth == 100) && (m_RegionHeight == 100) &&
		((point.x - x0) >= rect.left) &&
		((point.y - y0) >= rect.top) &&
		((point.x - x0) < rect.right) &&
		((point.y - y0) < rect.bottom))
	{
		UpdateData(TRUE);
		CRGNData *ptr = m_RGNDataArray[m_DisplayedRgnIndex - 1];
		ptr->SetImages(m_RedRegionImage, m_GreenRegionImage, m_BlueRegionImage);
		double xRatio = ((double)m_RegionWidth) / (rect.right - rect.left);
		double yRatio = ((double)m_RegionHeight) / (rect.bottom - rect.top);
		int x = (int)((point.x - x0) * xRatio);
		int y = (int)((point.y - y0) * yRatio);
		ptr->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
		ptr->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
		ptr->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
		ptr->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
		ptr->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
		ptr->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
		ptr->SetThreshold(RED_COLOR, (m_CTCParams.m_CTCParams[(int)PARAM_RED_THRESHOLD] + m_RedIntensity));
		ptr->SetThreshold(GREEN_COLOR, (m_CTCParams.m_CTCParams[(int)PARAM_GREEN_THRESHOLD] + m_GreenIntensity));
		ptr->SetThreshold(BLUE_COLOR, (m_CTCParams.m_CTCParams[(int)PARAM_BLUE_THRESHOLD] + m_BlueIntensity));
		ptr->SetCPI(RED_COLOR, m_RedIntensity);
		ptr->SetCPI(GREEN_COLOR, m_GreenIntensity);
		ptr->SetCPI(BLUE_COLOR, m_BlueIntensity);
		unsigned int color = ptr->GetColorCode();
		bool reviewAnswer[REVIEW_ANSWERS];
		memcpy(&reviewAnswer[NONCK], &ptr->m_ReviewAnswer[NONCK], sizeof(bool)*REVIEW_ANSWERS);
		m_HitFinder.ProcessOneRegion(&m_CTCParams, ptr, m_ZeissData);
		int itemIndex = 0;
		int idx = 0;
		CString labelStr;
		labelStr = _T("RedRelative");
		idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
		labelStr.Format(_T("%d"), ptr->m_RedValue);
		m_CellScoreList.SetItemText(idx, 1, labelStr);
		labelStr = _T("GreenRelative");
		idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
		labelStr.Format(_T("%d"), ptr->m_GreenValue);
		m_CellScoreList.SetItemText(idx, 1, labelStr);
		labelStr = _T("Ratio1:");
		idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
		labelStr.Format(_T("%.2lf"), ptr->m_Ratio1);
		m_CellScoreList.SetItemText(idx, 1, labelStr);
		labelStr.Format(_T(">Threshold1(=%0.2lf)"), (double)m_CTCParams.m_GroupParams[(int)PARAM_COEF1_THRESHOLD - (int)PARAM_FLOAT_FIRST]);
		idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
		if (ptr->m_Ratio1 > (double)m_CTCParams.m_GroupParams[(int)PARAM_COEF1_THRESHOLD - (int)PARAM_FLOAT_FIRST])
		{
			labelStr.Format(_T("YES"));
			m_CellScoreList.SetItemText(idx, 1, labelStr);
		}
		else
		{
			labelStr.Format(_T("No"));
			m_CellScoreList.SetItemText(idx, 1, labelStr);
		}
		if (ptr->m_Ratio1 > (double)m_CTCParams.m_GroupParams[(int)PARAM_COEF1_THRESHOLD - (int)PARAM_FLOAT_FIRST])
		{
			labelStr = _T("Ratio2");
			idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
			double nominator = m_CTCParams.m_GroupParams[(int)PARAM_NOMINATOR_R_COEF2 - (int)PARAM_FLOAT_FIRST] * ptr->m_RedValue
				+ m_CTCParams.m_GroupParams[(int)PARAM_NOMINATOR_G_COEF2 - (int)PARAM_FLOAT_FIRST] * ptr->m_GreenValue;
			double denominator = m_CTCParams.m_GroupParams[(int)PARAM_DENOMINATOR_R_COEF2 - (int)PARAM_FLOAT_FIRST] * ptr->m_RedValue
				+ m_CTCParams.m_GroupParams[(int)PARAM_DENOMINATOR_G_COEF2 - (int)PARAM_FLOAT_FIRST] * ptr->m_GreenValue + 1.0;
			if ((nominator > 0) && (denominator > 0))
				ptr->m_Ratio2 = nominator / denominator;
			else if (nominator <= 0)
				ptr->m_Ratio2 = 0;
			else
				ptr->m_Ratio2 = nominator;
			labelStr.Format(_T("%.2lf"), ptr->m_Ratio2);
			m_CellScoreList.SetItemText(idx, 1, labelStr);
			labelStr.Format(_T("<Threshold(=%0.2lf)"), (double)m_CTCParams.m_GroupParams[(int)PARAM_COEF2_THRESHOLD - (int)PARAM_FLOAT_FIRST]);
			idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
			if (ptr->m_Ratio2 < (double)m_CTCParams.m_GroupParams[(int)PARAM_COEF2_THRESHOLD - (int)PARAM_FLOAT_FIRST])
			{
				labelStr.Format(_T("YES"));
				m_CellScoreList.SetItemText(idx, 1, labelStr);
			}
			else
			{
				labelStr.Format(_T("No"));
				m_CellScoreList.SetItemText(idx, 1, labelStr);
			}
		}

		labelStr = _T("GreenCutoff");
		idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
		labelStr.Format(_T("%d"), m_CTCParams.m_WBCGreenRelative + m_CTCParams.m_CTCParams[(int)PARAM_WBCGREENMARKUP]);
		m_CellScoreList.SetItemText(idx, 1, labelStr);
		labelStr.Format(_T(">GreenCutoff(=%d)"), m_CTCParams.m_WBCGreenRelative + m_CTCParams.m_CTCParams[(int)PARAM_WBCGREENMARKUP]);
		idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
		if (ptr->m_GreenValue > (m_CTCParams.m_WBCGreenRelative + m_CTCParams.m_CTCParams[(int)PARAM_WBCGREENMARKUP]))
		{
			labelStr.Format(_T("YES"));
			m_CellScoreList.SetItemText(idx, 1, labelStr);
		}
		else
		{
			labelStr.Format(_T("No"));
			m_CellScoreList.SetItemText(idx, 1, labelStr);
		}
		labelStr = _T("Pixels(Cell Region)");
		idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
		labelStr.Format(_T("%d"), ptr->GetPixels(RB_COLORS));
		m_CellScoreList.SetItemText(idx, 1, labelStr);
		int greenCutoffValue = m_CTCParams.m_WBCGreenRelative + m_CTCParams.m_CTCParams[(int)PARAM_WBCGREENMARKUP] + m_GreenIntensity;
		int greenPixelCount = 0;
		if (ptr->GetBlobData(RB_COLORS)->size() > 0)
		{
			for (int i = 0; i < (int)(*ptr->GetBlobData(RB_COLORS))[0]->m_Pixels->size(); i++)
			{
				int position = (*(*ptr->GetBlobData(RB_COLORS))[0]->m_Pixels)[i];
				int greenIntensity = m_GreenRegionImage[position];
				if (greenIntensity > greenCutoffValue)
				{
					greenPixelCount++;
				}
			}
		}
		labelStr = _T("Pixels(>GreenCutoff)");
		idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
		labelStr.Format(_T("%d"), greenPixelCount);
		m_CellScoreList.SetItemText(idx, 1, labelStr);

		memcpy(&ptr->m_ReviewAnswer[NONCK], &reviewAnswer[NONCK], sizeof(bool)*REVIEW_ANSWERS);
		ptr->NullImages();
		blue = m_BlueRegionImage;
		green = m_GreenRegionImage;
		red = m_RedRegionImage;
		if (m_Image != NULL)
		{
			m_Image.Destroy();
			m_ImageWidth = 0;
			m_ImageHeight = 0;
		}
		m_ImageWidth = m_RegionWidth;
		m_ImageHeight = m_RegionHeight;
		m_Image.Create(m_ImageWidth, -m_ImageHeight, 24);
		CopyToRGBImage((m_Blue.GetCheck() ? blue : NULL), (m_Green.GetCheck() ? green : NULL), (m_Red.GetCheck() ? red : NULL), &m_Image, ptr);
		RECT rect1;
		m_ImageDisplay.GetWindowRect(&rect1);
		ScreenToClient(&rect1);
		InvalidateRect(&rect1, false);
	}
	else if ((m_RegionWidth != 100) || (m_RegionHeight != 100))
	{
		CString msg;
		msg.Format(_T("HitFinding using RightMouseClick only works when ImageWidth=ImageHeight=100 Pixels (ImageWidth=%d,ImageHeight=%d)"), m_RegionWidth, m_RegionHeight);
		AfxMessageBox(msg);
	}
	CDialogEx::OnRButtonDown(nFlags, point);
	EnableButtons(TRUE);
}

void CCTCViewerDlg::OnBnClickedResetcontrast()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	m_BlueSlider.SetPos(120);
	m_GreenSlider.SetPos(120);
	m_RedSlider.SetPos(120);
	if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
		DisplayROI(m_DisplayedRgnIndex);
}

int CCTCViewerDlg::getCellSize(BYTE *image, int width, int height)
{
	int size = 0;
	int maxI = 0;
	int minI = height;
	int maxJ = 0;
	int minJ = width;

	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			if (image[width * i + j] > 0)
			{
				if (i > maxI)
					maxI = i;
				if (i < minI)
					minI = i;
				if (j > maxJ)
					maxJ = j;
				if (j < minJ)
					minJ = j;
			}
		}
	}

	if ((maxI - minI) > (maxJ - minJ))
	{
		size = maxI - minI;
	}
	else
	{
		size = maxJ - minJ;
	}
	return size;
}

void CCTCViewerDlg::Get8BitImageData(CImage *rgb, CImage *red, CImage *green, CImage *blue)
{
	BYTE *pCursor = (BYTE*)rgb->GetBits();
	BYTE *pRedCursor = (BYTE*)red->GetBits();
	BYTE *pGreenCursor = (BYTE*)green->GetBits();
	BYTE *pBlueCursor = (BYTE*)blue->GetBits();
	int nHeight = m_ImageHeight;
	int nWidth = m_ImageWidth;
	int BlueMax = m_BlueSlider.GetPos();
	int GreenMax = m_GreenSlider.GetPos();
	int RedMax = m_RedSlider.GetPos();
	int nStride = rgb->GetPitch() - (nWidth * 3);
	UpdateData(TRUE);
	unsigned short *pBlueBuffer = m_BlueRegionImage;
	unsigned short *pGreenBuffer = m_GreenRegionImage;
	unsigned short *pRedBuffer = m_RedRegionImage;

	for (int y = 0; y<nHeight; y++)
	{
		for (int x = 0; x<nWidth; x++)
		{
			BYTE redValue = GetContrastEnhancedByte(*pRedBuffer++, RedMax, RED_COLOR, m_RedIntensity);
			BYTE greenValue = GetContrastEnhancedByte(*pGreenBuffer++, GreenMax, GREEN_COLOR, m_GreenIntensity);
			BYTE blueValue = GetContrastEnhancedByte(*pBlueBuffer++, BlueMax, BLUE_COLOR, m_BlueIntensity);
			*pCursor++ = blueValue;
			*pCursor++ = greenValue;
			*pCursor++ = redValue;
			*pBlueCursor++ = blueValue;
			*pBlueCursor++ = (BYTE) 0;
			*pBlueCursor++ = (BYTE) 0;
			*pGreenCursor++ = (BYTE) 0;
			*pGreenCursor++ = greenValue;
			*pGreenCursor++ = (BYTE) 0;
			*pRedCursor++ = (BYTE) 0;
			*pRedCursor++ = (BYTE) 0;
			*pRedCursor++ = redValue;
					
		}
		if (nStride > 0)
		{
			pCursor += nStride;
			pBlueCursor += nStride;
			pGreenCursor += nStride;
			pRedCursor += nStride;
		}
	}
}

BOOL CCTCViewerDlg::Save4BMPFiles(CString filename)
{
	BOOL ret = FALSE;
	CImage rgb;
	CImage red;
	CImage green;
	CImage blue;

	rgb.Create(m_RegionWidth, -m_RegionHeight, 24);
	red.Create(m_RegionWidth, -m_RegionHeight, 24);
	green.Create(m_RegionWidth, -m_RegionHeight, 24);
	blue.Create(m_RegionWidth, -m_RegionHeight, 24);
	Get8BitImageData(&rgb, &red, &green, &blue);

	char filename1[512];
	size_t size;
	wcstombs_s(&size, filename1, filename.GetBuffer(), filename.GetLength());
	char *ptr = strstr(filename1, ".bmp");
	if (ptr != NULL)
	{
		*ptr = '\0';
		CString pathname = CString(filename1);
		CString pathname1;
		pathname1.Format(_T("%s_RGB.bmp"), pathname);
		SaveBMPImage(pathname1, &rgb);
		pathname1.Format(_T("%s_RED.bmp"), pathname);
		SaveBMPImage(pathname1, &red);
		pathname1.Format(_T("%s_GREEN.bmp"), pathname);
		SaveBMPImage(pathname1, &green);
		pathname1.Format(_T("%s_BLUE.bmp"), pathname);
		SaveBMPImage(pathname1, &blue);
		ret = TRUE;
	}
	return ret;
}


void CCTCViewerDlg::OnBnClickedUsebox()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);
	mouseIndex = 0;
	m_CellScoreList.DeleteAllItems();
		
	m_Description = _T("");
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	unsigned short *blue = m_BlueRegionImage;
	unsigned short *green = m_GreenRegionImage;
	unsigned short *red = m_RedRegionImage;
	if (m_Image != NULL)
	{
		m_Image.Destroy();
		m_ImageWidth = 0;
		m_ImageHeight = 0;
	}
	m_ImageWidth = m_RegionWidth;
	m_ImageHeight = m_RegionHeight;
	m_Image.Create(m_ImageWidth, -m_ImageHeight, 24);

	CopyToRGBImage((m_Blue.GetCheck() ? blue : NULL), (m_Green.GetCheck() ? green : NULL), (m_Red.GetCheck() ? red : NULL), &m_Image,
		m_RGNDataArray[m_DisplayedRgnIndex-1]);
	RECT rect1;
	m_ImageDisplay.GetWindowRect(&rect1);
	ScreenToClient(&rect1);
	InvalidateRect(&rect1, false);
	EnableButtons(TRUE);
}


void CCTCViewerDlg::OnBnClickedReload()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	CFileDialog dlg(TRUE,    // open
		CString(".txt"),    // with default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("CTCParam files (*.txt)|*.txt||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		m_CTCParams.LoadCTCParams(dlg.GetPathName(), &m_TwoThresholds);
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
}

void CCTCViewerDlg::OnBnClickedSavedata()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	UpdateData(TRUE);
	CFileDialog dlg(FALSE,    // save
		CString(".txt"),    // with default extension
		NULL,    // no initial file name
		OFN_OVERWRITEPROMPT,
		_T("CTCParam files (*.txt)|*.txt||"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		m_CTCParams.SaveCTCParams(dlg.GetPathName());
	}
}

void CCTCViewerDlg::AutoScreen(int index)
{
	m_CellScoreList.DeleteAllItems();
	CRGNData *ptr = m_RGNDataArray[index];
	ptr->SetImages(m_RedRegionImage, m_GreenRegionImage, m_BlueRegionImage);
	ptr->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
	ptr->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
	ptr->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
	ptr->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
	ptr->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
	ptr->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
	ptr->SetThreshold(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_THRESHOLD]);
	ptr->SetThreshold(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_THRESHOLD]);
	ptr->SetThreshold(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_THRESHOLD]);
	ptr->SetCPI(RED_COLOR, m_RedIntensity);
	ptr->SetCPI(GREEN_COLOR, m_GreenIntensity);
	ptr->SetCPI(BLUE_COLOR, m_BlueIntensity);
	unsigned int color = ptr->GetColorCode();
	bool reviewAnswer[REVIEW_ANSWERS];
	memcpy(&reviewAnswer[NONCK], &ptr->m_ReviewAnswer[NONCK], sizeof(bool)*REVIEW_ANSWERS);
	m_HitFinder.ProcessOneRegion(&m_CTCParams, ptr, m_ZeissData);
	int itemIndex = 0;
	int idx = 0;
	CString labelStr;
	labelStr = _T("RedRelative");
	idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%d"), ptr->m_RedValue);
	m_CellScoreList.SetItemText(idx, 1, labelStr);
	labelStr = _T("GreenRelative");
	idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%d"), ptr->m_GreenValue);
	m_CellScoreList.SetItemText(idx, 1, labelStr);
	labelStr = _T("Ratio1:");
	idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%.2lf"), ptr->m_Ratio1);
	m_CellScoreList.SetItemText(idx, 1, labelStr);
	labelStr.Format(_T(">Threshold1(=%0.2lf)"), (double)m_CTCParams.m_GroupParams[(int)PARAM_COEF1_THRESHOLD - (int)PARAM_FLOAT_FIRST]);
	idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
	if (ptr->m_Ratio1 > (double)m_CTCParams.m_GroupParams[(int)PARAM_COEF1_THRESHOLD - (int)PARAM_FLOAT_FIRST])
	{
		labelStr.Format(_T("YES"));
		m_CellScoreList.SetItemText(idx, 1, labelStr);
	}
	else
	{
		labelStr.Format(_T("No"));
		m_CellScoreList.SetItemText(idx, 1, labelStr);
	}
	if (ptr->m_Ratio1 > (double)m_CTCParams.m_GroupParams[(int)PARAM_COEF1_THRESHOLD - (int)PARAM_FLOAT_FIRST])
	{
		labelStr = _T("Ratio2");
		idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
		double nominator = m_CTCParams.m_GroupParams[(int)PARAM_NOMINATOR_R_COEF2 - (int)PARAM_FLOAT_FIRST] * ptr->m_RedValue
			+ m_CTCParams.m_GroupParams[(int)PARAM_NOMINATOR_G_COEF2 - (int)PARAM_FLOAT_FIRST] * ptr->m_GreenValue;
		double denominator = m_CTCParams.m_GroupParams[(int)PARAM_DENOMINATOR_R_COEF2 - (int)PARAM_FLOAT_FIRST] * ptr->m_RedValue
			+ m_CTCParams.m_GroupParams[(int)PARAM_DENOMINATOR_G_COEF2 - (int)PARAM_FLOAT_FIRST] * ptr->m_GreenValue + 1.0;
		if ((nominator > 0) && (denominator > 0))
			ptr->m_Ratio2 = nominator / denominator;
		else if (nominator <= 0)
			ptr->m_Ratio2 = 0;
		else
			ptr->m_Ratio2 = nominator;
		labelStr.Format(_T("%.2lf"), ptr->m_Ratio2);
		m_CellScoreList.SetItemText(idx, 1, labelStr);
		labelStr.Format(_T("<Threshold2(=%0.2lf)"), (double)m_CTCParams.m_GroupParams[(int)PARAM_COEF2_THRESHOLD - (int)PARAM_FLOAT_FIRST]);
		idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
		if (ptr->m_Ratio2 < (double)m_CTCParams.m_GroupParams[(int)PARAM_COEF2_THRESHOLD - (int)PARAM_FLOAT_FIRST])
		{
			labelStr.Format(_T("YES"));
			m_CellScoreList.SetItemText(idx, 1, labelStr);
		}
		else
		{
			labelStr.Format(_T("No"));
			m_CellScoreList.SetItemText(idx, 1, labelStr);
		}
	}
	labelStr = _T("GreenCutoff");
	idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%d"), m_CTCParams.m_WBCGreenRelative + m_CTCParams.m_CTCParams[(int)PARAM_WBCGREENMARKUP]);
	m_CellScoreList.SetItemText(idx, 1, labelStr);
	labelStr.Format(_T(">GreenCutoff(=%d)"), m_CTCParams.m_WBCGreenRelative + m_CTCParams.m_CTCParams[(int)PARAM_WBCGREENMARKUP]);
	idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
	if (ptr->m_GreenValue > (m_CTCParams.m_WBCGreenRelative + m_CTCParams.m_CTCParams[(int)PARAM_WBCGREENMARKUP]))
	{
		labelStr.Format(_T("YES"));
		m_CellScoreList.SetItemText(idx, 1, labelStr);
	}
	else
	{
		labelStr.Format(_T("No"));
		m_CellScoreList.SetItemText(idx, 1, labelStr);
	}
	labelStr = _T("Pixels(Cell Region)");
	idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%d"), ptr->GetPixels(RB_COLORS));
	m_CellScoreList.SetItemText(idx, 1, labelStr);
	int greenCutoffValue = m_CTCParams.m_WBCGreenRelative + m_CTCParams.m_CTCParams[(int)PARAM_WBCGREENMARKUP] + m_GreenIntensity;
	int greenPixelCount = 0;
	if (ptr->GetBlobData(RB_COLORS)->size() > 0)
	{
		for (int i = 0; i < (int)(*ptr->GetBlobData(RB_COLORS))[0]->m_Pixels->size(); i++)
		{
			int position = (*(*ptr->GetBlobData(RB_COLORS))[0]->m_Pixels)[i];
			int greenIntensity = m_GreenRegionImage[position];
			if (greenIntensity > greenCutoffValue)
			{
				greenPixelCount++;
			}
		}
	}
	labelStr = _T("Pixels(>GreenCutoff)");
	idx = m_CellScoreList.InsertItem(itemIndex++, labelStr);
	labelStr.Format(_T("%d"), greenPixelCount);
	m_CellScoreList.SetItemText(idx, 1, labelStr);
	m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF] = m_CTCParams.m_WBCGreenRelative + m_CTCParams.m_CTCParams[(int)PARAM_WBCGREENMARKUP];
	UpdateData(FALSE);
	ptr->SetColorCode(color);
	memcpy(&ptr->m_ReviewAnswer[NONCK], &reviewAnswer[NONCK], sizeof(bool)*REVIEW_ANSWERS);
	ptr->NullImages();
}

void CCTCViewerDlg::FreeRegionImageData()
{
	if (m_RedRegionImage != NULL)
	{
		delete[] m_RedRegionImage;
		m_RedRegionImage = NULL;
	}
	if (m_GreenRegionImage != NULL)
	{
		delete[] m_GreenRegionImage;
		m_GreenRegionImage = NULL;
	}
	if (m_BlueRegionImage != NULL)
	{
		delete[] m_BlueRegionImage;
		m_BlueRegionImage = NULL;
	}
	m_RegionX0 = 0;
	m_RegionY0 = 0;
	m_RegionWidth = 0;
	m_RegionHeight = 0;
}

LRESULT CCTCViewerDlg::OnMyMessage(WPARAM wparam, LPARAM lparam)
{
	UpdateData(FALSE);
	return 0;
}

LRESULT CCTCViewerDlg::OnMyMessage2(WPARAM wparam, LPARAM lparam)
{
	EnableButtons(TRUE);
	return 0;
}

void CCTCViewerDlg::ReadComments(CString filename)
{
	m_Comments.clear();
	CStdioFile theFile;
	if (theFile.Open(filename, CFile::modeRead | CFile::typeText))
	{
		CString textline;
		while (theFile.ReadString(textline))
		{
			CString comment = textline;
			m_Comments.push_back(comment);
		}
		theFile.Close();
	}
}

void CCTCViewerDlg::SaveComments(CString filename)
{
	CStdioFile theFile;
	if (theFile.Open(filename, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		for (int i = 0; i < (int)m_Comments.size(); i++)
		{
			CString comment;
			comment.Format(_T("%s\n"), m_Comments[i]);
			theFile.WriteString(comment);
		}
		theFile.Close();
	}
}

void CCTCViewerDlg::DisplayComment(int regionIndex)
{
	m_Comment = _T("");
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	if ((regionIndex > 0) && (regionIndex <= m_CTCCount))
	{
		for (int i = 0; i < (int)m_Comments.size(); i++)
		{
			CString textline = m_Comments[i];
			int index = textline.Find(',');
			if (index != -1)
			{
				CString numStr = textline.Mid(0, index);
				int rgnNum = _wtoi(numStr);
				if (rgnNum == regionIndex)
				{
					m_Comment = textline.Mid(index + 1);
					::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
					break;
				}
			}
		}
	}
}

bool CCTCViewerDlg::ReadHitFile(CString filename)
{
	bool ret = false;
	CString message;
	CFile theFile;
	if (theFile.Open(filename, CFile::modeRead))
	{
		CArchive archive(&theFile, CArchive::load);
		int version = 0;
		archive >> version;
		if (version != HIT_FILE_VERSION)
		{
			m_Status.Format(_T("Hit File Version Number (=%d) != %d"), version, HIT_FILE_VERSION);
			UpdateData(FALSE);
			message.Format(_T("Hit File Version Number (=%d) != %d"), version, HIT_FILE_VERSION);
			m_Log.Message(message);
		}
		else
		{
			int totalCount = 0;
			archive >> totalCount;
			if (totalCount > 0)
			{
				int isZeissData;
				archive >> isZeissData;
				if (isZeissData == 0)
					m_ZeissData = false;
				else
					m_ZeissData = true;
				unsigned short CPI_Value;
				archive >> CPI_Value;
				m_RedIntensity = CPI_Value;
				archive >> CPI_Value;
				m_GreenIntensity = CPI_Value;
				archive >> CPI_Value;
				m_BlueIntensity = CPI_Value;
				int WBCAverage;
				archive >> WBCAverage;
				m_CTCParams.m_WBCGreenRelative = WBCAverage - m_GreenIntensity;
				for (int i = 0; i < totalCount; i++)
				{
					CSingleRegionImageData *data = new CSingleRegionImageData();
					data->Serialize(archive);
					CRGNData *ptr = new CRGNData(data);
					ptr->m_HitIndex = data->m_HitIndex + 1;
					ptr->SetColorCode(data->m_ColorCode);
					data->m_RedRegionImage = NULL;
					data->m_GreenRegionImage = NULL;
					data->m_BlueRegionImage = NULL;
					delete data;
					ptr->SetCutoff(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF]);
					ptr->SetCutoff(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF]);
					ptr->SetCutoff(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF]);
					ptr->SetContrast(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST]);
					ptr->SetContrast(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST]);
					ptr->SetContrast(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST]);
					ptr->SetThreshold(RED_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_RED_THRESHOLD]);
					ptr->SetThreshold(GREEN_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_GREEN_THRESHOLD]);
					ptr->SetThreshold(BLUE_COLOR, m_CTCParams.m_CTCParams[(int)PARAM_BLUE_THRESHOLD]);
					ptr->SetCPI(RED_COLOR, m_RedIntensity);
					ptr->SetCPI(GREEN_COLOR, m_GreenIntensity);
					ptr->SetCPI(BLUE_COLOR, m_BlueIntensity);
					m_RGNDataArray.push_back(ptr);
				}
			}
			else
			{
				m_Status.Format(_T("NumHits (=%d) = 0"), totalCount);
				UpdateData(FALSE);
			}
		}
		archive.Close();
		theFile.Close();
		m_CTCCount = (int)m_RGNDataArray.size();
		if (m_CTCCount > 0)
			ret = true;
		UpdateData(FALSE);
	}
	return ret;
}
bool CCTCViewerDlg::SaveHitFile(CString filename)
{
	// TODO:  在此加入控制項告知處理常式程式碼
	bool ret = false;
	CString message;

	CFile theFile;
	if (theFile.Open(filename, CFile::modeCreate | CFile::modeWrite))
	{
		CArchive archive(&theFile, CArchive::store);
		archive << (int)4; // Version Number
		archive << (int)m_RGNDataArray.size();
		if (m_ZeissData)
			archive << 1;
		else
			archive << 0;
		archive << m_RedTIFFData->GetCPI();
		archive << m_GreenTIFFData->GetCPI();
		archive << m_BlueTIFFData->GetCPI();
		archive << m_CTCParams.m_WBCGreenRelative + m_GreenTIFFData->GetCPI();
		bool status = false;
		unsigned short *redImg = new unsigned short[sizeof(unsigned short) * 300 * 300];
		unsigned short *greenImg = new unsigned short[sizeof(unsigned short) * 300 * 300];
		unsigned short *blueImg = new unsigned short[sizeof(unsigned short) * 300 * 300];
		for (int i = 0; i < (int)m_RGNDataArray.size(); i++)
		{
			CSingleRegionImageData *data = new CSingleRegionImageData();
			CRGNData *ptr = m_RGNDataArray[i];
			unsigned int color = ptr->GetColorCode();
			int x0, y0;
			ptr->GetPosition(&x0, &y0);
			data->m_HitIndex = i;
			data->m_ColorCode = color;
			data->m_RegionWidth = 300;
			data->m_RegionHeight = 300;
			data->m_RegionX0Pos = x0 - 100;
			data->m_RegionY0Pos = y0 - 100;
			m_RedTIFFData->GetImageRegion(data->m_RegionX0Pos, data->m_RegionY0Pos, 300, 300, redImg);
			m_GreenTIFFData->GetImageRegion(data->m_RegionX0Pos, data->m_RegionY0Pos, 300, 300, greenImg);
			m_BlueTIFFData->GetImageRegion(data->m_RegionX0Pos, data->m_RegionY0Pos, 300, 300, blueImg);
			data->m_RedRegionImage = redImg;
			data->m_GreenRegionImage = greenImg;
			data->m_BlueRegionImage = blueImg;
			data->m_Comment = GetCommentContents(i + 1);
			data->Serialize(archive);
			data->m_RedRegionImage = NULL;
			data->m_GreenRegionImage = NULL;
			data->m_BlueRegionImage = NULL;
			delete data;
		}
		archive.Close();
		theFile.Close();
		delete[] redImg;
		delete[] greenImg;
		delete[] blueImg;
		if (status)
		{
			message.Format(_T("Saved %u Hits to Hit Archive File %s"), m_RGNDataArray.size(), filename);
			m_Log.Message(message);
			ret = true;
		}
	}
	else
	{
		message.Format(_T("Failed to open %s"), filename);
		m_Status = message;
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		m_Log.Message(message);
	}	
	return ret;
}
void CCTCViewerDlg::OnBnClickedOpenrgn()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	if (m_UsingHitImageData)
	{
		AfxMessageBox(_T("Loading .Hit File Prevented from Loading .rgn file"));
		return;
	}
	FreeRGNData(&m_RGNDataArray);
	m_CellScoreList.DeleteAllItems();
	m_CTCCount = 0;
	m_DisplayedRgnIndex = 0;
	m_CTCIndex.SetWindowTextW(_T("0"));
	m_ConfirmedCTCNum = 0;
	m_ConfirmedNonCTCNum = 0;
	m_CTCNum = 0;
	m_CommentFileName = _T("");
	m_Comment = _T("");
	::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	m_RedIntensity = m_RedTIFFData->GetCPI();
	m_GreenIntensity = m_GreenTIFFData->GetCPI();
	m_BlueIntensity = m_BlueTIFFData->GetCPI();
	CString message;
	for (int i = 0; i < (int)PARAM_LASTONE; i++)
	{
		message.Format(_T("%s=%d"), m_CTCParams.m_CTCParamNames[i], m_CTCParams.m_CTCParams[i]);
		m_Log.Message(message);
	}

	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("RGN files (*.rgn)|*.rgn"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		m_CommentFileName = filename;
		WCHAR *char1 = _T(".");
		int commentFileIndex = m_CommentFileName.ReverseFind(*char1);
		m_CommentFileName = m_CommentFileName.Mid(0, commentFileIndex + 1) + _T("txt");
		CString wbcFilename = filename;
		commentFileIndex = wbcFilename.ReverseFind(*char1);
		m_RGNFilename = dlg.GetFileName();
		if (LoadRegionData(filename))
		{
			message.Format(_T("Region File %s loaded successfully, #Hits=%d"), filename, m_CTCCount);
			m_Log.Message(message);
			CountColorCode();
			m_CTCCount = (int)m_RGNDataArray.size();
			if (m_CTCCount > 0)
			{
				ReadComments(m_CommentFileName);
				LoadCheckListData(m_CommentFileName);
				StartFromHeadOfList();
			}
			else
			{
				m_Status.Format(_T("# of Regions=%d in RGN File %s"), m_CTCCount, m_RGNFilename);
				::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
			}
		}
		else
		{
			m_Status.Format(_T("Failed to load RGN File %s"), m_RGNFilename);
			::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		}
	}
	else
	{
		m_Status = _T("Failed to provide RGN File Name");
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
	}
}

void CCTCViewerDlg::OnBnClickedAddone()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	if (m_UsingHitImageData)
	{
		AfxMessageBox(_T("Loading .Hit File Prevented from Adding One CTC"));
		return;
	}
	EnableButtons(FALSE);
	if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
	{
		int x0, y0;
		m_RGNDataArray[m_DisplayedRgnIndex - 1]->GetPosition(&x0, &y0);
		CString message;
		message.Format(_T("This Confirmed CTC is located at (%d,%d)"), x0 + 50, y0 + 50);
		AddOneCTCDlg *dlg = new AddOneCTCDlg(message);
		INT_PTR result = dlg->DoModal();
		if (result == IDOK)
		{
			CRGNData *ptr = new CRGNData(x0, y0, PATCH_WIDTH, PATCH_HEIGHT);
			ptr->SetColorCode(CTC2);
			ptr->m_ReviewAnswer[NONCK] = false;
			ptr->m_ReviewAnswer[CONFIRMED] = true;
			ptr->m_ReviewAnswer[WBCSHAPE] = false;
			m_RGNDataArray.push_back(ptr);
			m_CTCCount = (int)m_RGNDataArray.size();
			CountColorCode();
			UpdateData(FALSE);
		}
		delete dlg;
	}
	else
	{
		AfxMessageBox(_T("Incorrect Region No. or Rank No. Displayed"));
	}
	EnableButtons(TRUE);
}

void CCTCViewerDlg::EnableButtons(BOOL enable)
{
	UpdateData(FALSE);
	CButton *btn = (CButton *)GetDlgItem(IDC_BLUE);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_RED);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_GREEN);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_NEXT);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_PREV);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_SAVERGN);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_NONCK);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_WBCS);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_CONFIRMEDCTC);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_SELECT);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_SAVEIMAGE);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_REDRESET);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_GREENRESET);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_BLUERESET);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_REDFILE);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_RESETCONTRAST);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_USEBOX);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_RELOAD);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_SAVEDATA);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_OPENRGN);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_ADDONE);
	btn->EnableWindow(enable);
	btn = (CButton *)GetDlgItem(IDC_ZOOMIN);
	btn->EnableWindow(enable);
}

void CCTCViewerDlg::OnBnClickedForctconly()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);
	StartFromHeadOfList();
	EnableButtons(TRUE);
}


void CCTCViewerDlg::OnBnClickedForctc2only()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);
	StartFromHeadOfList();
	EnableButtons(TRUE);
}


void CCTCViewerDlg::OnBnClickedTwotypectcs()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);
	StartFromHeadOfList();
	EnableButtons(TRUE);
}


void CCTCViewerDlg::OnBnClickedNonctconly()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);
	StartFromHeadOfList();
	EnableButtons(TRUE);
}


void CCTCViewerDlg::OnBnClickedForallregions()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);
	StartFromHeadOfList();
	EnableButtons(TRUE);
}

void CCTCViewerDlg::StartFromHeadOfList()
{
	m_DisplayedRgnIndex = 0;
	bool result = GetNextIndex();
	if (result)
	{
		CString indexStr;
		indexStr.Format(_T("%d"), m_DisplayedRgnIndex);
		m_CTCIndex.SetWindowTextW(indexStr);
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		ResetMag1();
		LoadRegionImageFile(m_DisplayedRgnIndex - 1, m_Mag1.GetPos());
	}
	else
	{
		CString message;
		message.Format(_T("Failed to find one region that has specified region type. Please change Region Type for Reviewing"));
		m_Status = _T("Please Change Region Type for Reviewing");
		m_DisplayedRgnIndex = 0;
		m_CTCIndex.SetWindowTextW(_T("0"));
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		AfxMessageBox(message);
	}
}

void CCTCViewerDlg::OnBnClickedZoomin()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);
	m_Mag1.SetPos(MAX_MAGNIFICATION);
	if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
	{
		LoadRegionImageFile(m_DisplayedRgnIndex - 1, m_Mag1.GetPos());
	}
	EnableButtons(TRUE);
}

void CCTCViewerDlg::LoadCheckListData(CString filename)
{
	m_ReviewerNameInRGNFile = _T("temp");
	WCHAR *char1 = _T(".");
	CString filename1 = filename;
	int filename1Index = filename1.ReverseFind(*char1);
	CString checklistFilename = filename1.Mid(0, filename1Index + 1) + _T("at5");
	CFile theFile;
	if (theFile.Open(checklistFilename, CFile::modeRead))
	{
		CArchive archive(&theFile, CArchive::load);
		archive >> m_ReviewerNameInRGNFile;
		for (int i = 0; i < (int)m_RGNDataArray.size(); i++)
		{
			archive.Read(&m_RGNDataArray[i]->m_ReviewAnswer, sizeof(bool) * REVIEW_ANSWERS);
		}
		archive.Close();
		theFile.Close();
	}
	else
	{
		checklistFilename = filename1.Mid(0, filename1Index + 1) + _T("at4");
		if (theFile.Open(checklistFilename, CFile::modeRead))
		{
			CArchive archive(&theFile, CArchive::load);
			archive >> m_ReviewerNameInRGNFile;
			bool at4ReviewAnswer[4];
			for (int i = 0; i < (int)m_RGNDataArray.size(); i++)
			{
				archive.Read(&at4ReviewAnswer, sizeof(bool) * REVIEW_ANSWERS);
				memset(&m_RGNDataArray[i]->m_ReviewAnswer, 0, sizeof(bool) * REVIEW_ANSWERS);
				if (m_RGNDataArray[i]->GetColorCode() == CTC2)
					m_RGNDataArray[i]->m_ReviewAnswer[CONFIRMED] = true;
				else if (at4ReviewAnswer[0])
					m_RGNDataArray[i]->m_ReviewAnswer[NONCK] = true;
				else
					m_RGNDataArray[i]->m_ReviewAnswer[WBCSHAPE] = true;
			}
			archive.Close();
			theFile.Close();
		}
	}
}

void CCTCViewerDlg::SaveCheckListData(CString filename)
{
	CFile theFile;
	if (theFile.Open(filename, CFile::modeCreate | CFile::modeWrite))
	{
		CArchive archive(&theFile, CArchive::store);
		archive << m_ReviewerName;
		for (int i = 0; i < (int)m_RGNDataArray.size(); i++)
		{
			archive.Write(&m_RGNDataArray[i]->m_ReviewAnswer, sizeof(bool) * REVIEW_ANSWERS);
		}
		archive.Close();
		theFile.Close();
	}
}

void CCTCViewerDlg::OnBnClickedDeleteone()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
	{
		delete m_RGNDataArray[m_DisplayedRgnIndex - 1];
		m_RGNDataArray[m_DisplayedRgnIndex - 1] = NULL;
		vector<CRGNData *> cellList;
		for (int i = 0; i < m_CTCCount; i++)
		{
			if (m_RGNDataArray[i] != NULL)
			{
				cellList.push_back(m_RGNDataArray[i]);
				m_RGNDataArray[i] = NULL;
			}
		}
		m_RGNDataArray.clear();
		for (int i = 0; i < (int)cellList.size(); i++)
		{
			m_RGNDataArray.push_back(cellList[i]);
			cellList[i] = NULL;
		}
		cellList.clear();
		CountColorCode();
		m_CTCCount = (int)m_RGNDataArray.size();
		if (m_DisplayedRgnIndex > 1)
			m_DisplayedRgnIndex--;
		CString indexStr;
		indexStr.Format(_T("%d"), m_DisplayedRgnIndex);
		m_CTCIndex.SetWindowTextW(indexStr);
		::PostMessage(GetSafeHwnd(), WM_MY_MESSAGE, NULL, NULL);
		ResetMag1();
		LoadRegionImageFile(m_DisplayedRgnIndex - 1, m_Mag1.GetPos());
	}
}


void CCTCViewerDlg::OnBnClickedNonck()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);
	m_NonCKRadio.SetCheck(BST_CHECKED);
	if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
	{
		m_RGNDataArray[m_DisplayedRgnIndex - 1]->m_ReviewAnswer[NONCK] = true;
		m_RGNDataArray[m_DisplayedRgnIndex - 1]->m_ReviewAnswer[CONFIRMED] = false;
		m_RGNDataArray[m_DisplayedRgnIndex - 1]->m_ReviewAnswer[WBCSHAPE] = false;
		m_RGNDataArray[m_DisplayedRgnIndex - 1]->SetColorCode(NONCTC);
		UpdateColorCodeSelection();
	}
	EnableButtons(TRUE);
}


void CCTCViewerDlg::OnBnClickedWbcs()
{
	// TODO:  在此加入控制項告知處理常式程式碼
	EnableButtons(FALSE);
	m_WBCsRadio.SetCheck(BST_CHECKED);
	if ((m_DisplayedRgnIndex > 0) && (m_DisplayedRgnIndex <= m_CTCCount))
	{
		m_RGNDataArray[m_DisplayedRgnIndex - 1]->m_ReviewAnswer[NONCK] = false;
		m_RGNDataArray[m_DisplayedRgnIndex - 1]->m_ReviewAnswer[CONFIRMED] = false;
		m_RGNDataArray[m_DisplayedRgnIndex - 1]->m_ReviewAnswer[WBCSHAPE] = true;
		m_RGNDataArray[m_DisplayedRgnIndex - 1]->SetColorCode(NONCTC);
		UpdateColorCodeSelection();
	}
	EnableButtons(TRUE);
}
