// AddOneCTCDlg.cpp : 實作檔
//

#include "stdafx.h"
#include "AddOneCTCDlg.h"
#include "afxdialogex.h"


// AddOneCTCDlg 對話方塊

IMPLEMENT_DYNAMIC(AddOneCTCDlg, CDialog)

AddOneCTCDlg::AddOneCTCDlg(CString message, CWnd* pParent /*=NULL*/)
	: CDialog(AddOneCTCDlg::IDD, pParent)
{
	m_Message = message;
}

AddOneCTCDlg::~AddOneCTCDlg()
{
}

void AddOneCTCDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_MESSAGE, m_Message);
}


BEGIN_MESSAGE_MAP(AddOneCTCDlg, CDialog)
END_MESSAGE_MAP()


// AddOneCTCDlg 訊息處理常式
