#include "stdafx.h"
#include "SingleRegionImageData.h"

IMPLEMENT_SERIAL(CSingleRegionImageData, CObject, 4)

CSingleRegionImageData::CSingleRegionImageData()
{
	m_HitIndex = 0;
	m_ColorCode = 0;
	m_RedRegionImage = NULL;
	m_GreenRegionImage = NULL;
	m_BlueRegionImage = NULL;
	m_Comment = _T("");
}

CSingleRegionImageData::~CSingleRegionImageData()
{
	if (m_RedRegionImage != NULL)
	{
		delete[] m_RedRegionImage;
		m_RedRegionImage = NULL;
	}
	if (m_GreenRegionImage != NULL)
	{
		delete[] m_GreenRegionImage;
		m_GreenRegionImage = NULL;
	}
	if (m_BlueRegionImage != NULL)
	{
		delete[] m_BlueRegionImage;
		m_BlueRegionImage = NULL;
	}
}

void CSingleRegionImageData::Serialize(CArchive& archive)
{
	CObject::Serialize(archive);

	if (archive.IsStoring())
	{
		archive << m_HitIndex;
		archive << m_ColorCode;
		archive << m_RegionX0Pos;
		archive << m_RegionY0Pos;
		archive << m_RegionWidth;
		archive << m_RegionHeight;
		archive.Write(m_RedRegionImage, sizeof(unsigned short) * m_RegionWidth * m_RegionHeight);
		archive.Write(m_GreenRegionImage, sizeof(unsigned short) * m_RegionWidth * m_RegionHeight);
		archive.Write(m_BlueRegionImage, sizeof(unsigned short) * m_RegionWidth * m_RegionHeight);
		archive << m_Comment;
	}
	else
	{
		archive >> m_HitIndex;
		archive >> m_ColorCode;
		archive >> m_RegionX0Pos;
		archive >> m_RegionY0Pos;
		archive >> m_RegionWidth;
		archive >> m_RegionHeight;
		m_RedRegionImage = new unsigned short[m_RegionWidth * m_RegionHeight];
		archive.Read(m_RedRegionImage, sizeof(unsigned short) * m_RegionWidth * m_RegionHeight);
		m_GreenRegionImage = new unsigned short[m_RegionWidth * m_RegionHeight];
		archive.Read(m_GreenRegionImage, sizeof(unsigned short) * m_RegionWidth * m_RegionHeight);
		m_BlueRegionImage = new unsigned short[m_RegionWidth * m_RegionHeight];
		archive.Read(m_BlueRegionImage, sizeof(unsigned short) * m_RegionWidth * m_RegionHeight);
		archive >> m_Comment;
	}
}