#pragma once

typedef enum PIXEL_COLOR
{
	RB_COLORS,
	GB_COLORS,
	RED_COLOR,
	GREEN_COLOR,
	BLUE_COLOR
} PIXEL_COLOR_TYPE;

#define CTC 255
#define CTC2 16711935
#define NONCTC 16776960