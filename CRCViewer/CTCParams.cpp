#include "stdafx.h"
#include "CTCParams.h"
#include "string.h"

CCTCParams::CCTCParams()
{
	m_CTCParams[(int)PARAM_RED_CUTOFF] = 20;
	m_CTCParams[(int)PARAM_GREEN_CUTOFF] = 20;
	m_CTCParams[(int)PARAM_BLUE_CUTOFF] = 0;
	m_CTCParams[(int)PARAM_RED_CONTRAST] = 760;
	m_CTCParams[(int)PARAM_GREEN_CONTRAST] = 900;
	m_CTCParams[(int)PARAM_BLUE_CONTRAST] = 1000;
	m_CTCParams[(int)PARAM_RED_THRESHOLD] = 180;
	m_CTCParams[(int)PARAM_GREEN_THRESHOLD] = 40;
	m_CTCParams[(int)PARAM_BLUE_THRESHOLD] = 150;
	m_CTCParams[(int)PARAM_MAX_BLOBPIXELCOUNT] = 1500;
	m_CTCParams[(int)PARAM_MIN_BLOBPIXELCOUNT] = 100;
	m_CTCParams[(int)PARAM_BOUNDINGBOX_ASPECTRATIO] = 35;
	m_CTCParams[(int)PARAM_BOUNDINGBOX_PIXELCOUNTRATIO] = 35;
	m_CTCParams[(int)PARAM_REDTHRESHOLD_DROP] = 50;
	m_CTCParams[(int)PARAM_SATURATED_REDCOUNT] = 3000;
	m_CTCParams[(int)PARAM_SATURATED_BLUECOUNT] = 5000;
	m_CTCParams[(int)PARAM_INSIDEREDBLOB_BLUECOUNT] = 35;
	m_CTCParams[(int)PARAM_WBCGREENMARKUP] = 50;
	m_CTCParams[(int)PARAM_LASTONE] = 1;

	m_CTCParamNames[(int)PARAM_RED_CUTOFF] = _T("RedCutoff");
	m_CTCParamNames[(int)PARAM_GREEN_CUTOFF] = _T("GreenCutoff");
	m_CTCParamNames[(int)PARAM_BLUE_CUTOFF] = _T("BlueCutoff");;
	m_CTCParamNames[(int)PARAM_RED_CONTRAST] = _T("RedContrast");
	m_CTCParamNames[(int)PARAM_GREEN_CONTRAST] = _T("GreenContrast");
	m_CTCParamNames[(int)PARAM_BLUE_CONTRAST] = _T("BlueContrast");
	m_CTCParamNames[(int)PARAM_RED_THRESHOLD] = _T("RedBoundaryThreshold");
	m_CTCParamNames[(int)PARAM_GREEN_THRESHOLD] = _T("GreenBoundaryThreshold");
	m_CTCParamNames[(int)PARAM_BLUE_THRESHOLD] = _T("BlueBoundaryThreshold");
	m_CTCParamNames[(int)PARAM_MAX_BLOBPIXELCOUNT] = _T("MaxBlobPixelcount");
	m_CTCParamNames[(int)PARAM_MIN_BLOBPIXELCOUNT] = _T("MinBlobPixelcount");
	m_CTCParamNames[(int)PARAM_BOUNDINGBOX_ASPECTRATIO] = _T("BoundingBoxAspectRatio");
	m_CTCParamNames[(int)PARAM_BOUNDINGBOX_PIXELCOUNTRATIO] = _T("BoundingBoxPixelCountRatio");
	m_CTCParamNames[(int)PARAM_REDTHRESHOLD_DROP] = _T("RedThresholdDrop");
	m_CTCParamNames[(int)PARAM_SATURATED_REDCOUNT] = _T("SaturatedRedCount");
	m_CTCParamNames[(int)PARAM_SATURATED_BLUECOUNT] = _T("SaturatedBlueCount");
	m_CTCParamNames[(int)PARAM_INSIDEREDBLOB_BLUECOUNT] = _T("InsideRedBlobBlueCount");
	m_CTCParamNames[(int)PARAM_WBCGREENMARKUP] = _T("WBCGreenMarkup");
	m_CTCParamNames[(int)PARAM_LASTONE] = _T("LastParameter");

	m_WBCGreenRelative = 50;

	m_GroupParams[0] = 1.0F;
	m_GroupParams[(int)PARAM_NOMINATOR_R_COEF1 - (int)PARAM_FLOAT_FIRST] = 1.0F;
	m_GroupParams[(int)PARAM_NOMINATOR_G_COEF1 - (int)PARAM_FLOAT_FIRST] = 0.0F;
	m_GroupParams[(int)PARAM_DENOMINATOR_R_COEF1 - (int)PARAM_FLOAT_FIRST] = 0.0F;
	m_GroupParams[(int)PARAM_DENOMINATOR_G_COEF1 - (int)PARAM_FLOAT_FIRST] =1.0F;
	m_GroupParams[(int)PARAM_NOMINATOR_R_COEF2 - (int)PARAM_FLOAT_FIRST] = 1.0F;
	m_GroupParams[(int)PARAM_NOMINATOR_G_COEF2 - (int)PARAM_FLOAT_FIRST] = 0.0F;
	m_GroupParams[(int)PARAM_DENOMINATOR_R_COEF2 - (int)PARAM_FLOAT_FIRST] = 1.0F;
	m_GroupParams[(int)PARAM_DENOMINATOR_G_COEF2 - (int)PARAM_FLOAT_FIRST] = -1.0F;
	m_GroupParams[(int)PARAM_COEF1_THRESHOLD - (int)PARAM_FLOAT_FIRST] = 1.0F;
	m_GroupParams[(int)PARAM_COEF2_THRESHOLD - (int)PARAM_FLOAT_FIRST] = 2.0F;
	m_GroupParams[(int)PARAM_FLOAT_LAST - (int)PARAM_FLOAT_FIRST] = 1.0F;

	m_GroupNames[0] = _T("FirstFloatParameter");
	m_GroupNames[(int)PARAM_NOMINATOR_R_COEF1 - (int)PARAM_FLOAT_FIRST] = _T("Nominator_R_Coef1");
	m_GroupNames[(int)PARAM_NOMINATOR_G_COEF1 - (int)PARAM_FLOAT_FIRST] = _T("Nominator_G_Coef1");
	m_GroupNames[(int)PARAM_DENOMINATOR_R_COEF1 - (int)PARAM_FLOAT_FIRST] = _T("Denominator_R_Coef1");
	m_GroupNames[(int)PARAM_DENOMINATOR_G_COEF1 - (int)PARAM_FLOAT_FIRST] = _T("Denominator_G_Coef1");
	m_GroupNames[(int)PARAM_NOMINATOR_R_COEF2 - (int)PARAM_FLOAT_FIRST] = _T("Nominator_R_Coef2");
	m_GroupNames[(int)PARAM_NOMINATOR_G_COEF2 - (int)PARAM_FLOAT_FIRST] = _T("Nominator_G_Coef2");
	m_GroupNames[(int)PARAM_DENOMINATOR_R_COEF2 - (int)PARAM_FLOAT_FIRST] = _T("Denominator_R_Coef2");
	m_GroupNames[(int)PARAM_DENOMINATOR_G_COEF2 - (int)PARAM_FLOAT_FIRST] = _T("Denominator_G_Coef2");
	m_GroupNames[(int)PARAM_COEF1_THRESHOLD - (int)PARAM_FLOAT_FIRST] = _T("Coef1_Threshold");
	m_GroupNames[(int)PARAM_COEF2_THRESHOLD - (int)PARAM_FLOAT_FIRST] = _T("Coef2_Threshold");
	m_GroupNames[(int)PARAM_FLOAT_LAST - (int)PARAM_FLOAT_FIRST] = _T("LastFloatParameter");
}

void CCTCParams::LoadCTCParams(CString filename, CString *twoThresholds)
{
	CStdioFile theFile;
	if (theFile.Open(filename, CFile::modeRead | CFile::typeText))
	{
		CString textline;
		while (theFile.ReadString(textline))
		{
			bool found = false;
			for (int i = 0; i < (((int)PARAM_LASTONE) + 1); i++)
			{
				int index = textline.Find(m_CTCParamNames[i]);
				if (index > -1)
				{
					CString name(m_CTCParamNames[i]);
					index += name.GetLength() + 1;
					m_CTCParams[i] = _wtoi(textline.Mid(index));
					found = true;
					break;
				}
			}
			if (!found)
			{
				for (int i = 0; i < (((int)PARAM_FLOAT_LAST) - ((int)PARAM_FLOAT_FIRST) + 1); i++)
				{
					int index = textline.Find(m_GroupNames[i]);
					if (index > -1)
					{
						CString name(m_GroupNames[i]);
						index += name.GetLength() + 1;
						m_GroupParams[i] = (float) _wtof(textline.Mid(index));
						break;
					}
				}
			}
		}
		theFile.Close();
		textline.Format(_T("%.1f;%.1f"), m_GroupParams[(int)PARAM_COEF1_THRESHOLD - (int)PARAM_FLOAT_FIRST],
			m_GroupParams[(int)PARAM_COEF2_THRESHOLD - (int)PARAM_FLOAT_FIRST]);
		*twoThresholds = textline;
	}
}

void CCTCParams::SaveCTCParams(CString filename)
{
	CStdioFile theFile;
	if (theFile.Open(filename, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		CString textline;
		for (int i = 0; i < (((int)PARAM_LASTONE) + 1); i++)
		{
			textline.Format(_T("%s=%d\n"), m_CTCParamNames[i], m_CTCParams[i]);
			theFile.WriteString(textline);
		}
		for (int i = 0; i < (((int)PARAM_FLOAT_LAST) - ((int)PARAM_FLOAT_FIRST) + 1); i++)
		{
			textline.Format(_T("%s=%.1f\n"), m_GroupNames[i], m_GroupParams[i]);
			theFile.WriteString(textline);
		}
		theFile.Close();
	}
}